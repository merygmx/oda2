﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Reto3 : MonoBehaviour
{
    private Button btnaceptar3;
    private Button btnlupa;
    private Button btnflechaizq;
    private Button btnflechader;
    private Button btnreto4;
    private Button btnaceptarayu;
    private Button btnexitoo;
    private Button btnaceptarpierde;
    private Button btnaceptarintentar;
    private bool dragging;
    private bool flag;
	private bool reinicio = false;
    private bool gana;
	private bool intentar=false;

    private int ley;
	private int aux = 0;
	private int corazonesreto;
    private int contadorleyenda;
	private int opcionleyenda;
    private Vector3 posIni;
    private GameObject Reto3Obj;
    private GameObject Reto4Obj;
    private GameObject fondo;
    private GameObject popupexito;
    private GameObject popupintentar;
    private GameObject zonanaturaleza;
    private GameObject brillo_trofeo;
    private GameObject fondotrofeo;
    private GameObject popuppierdevidas;
    private GameObject zonalocalidad;
    private GameObject zonaheroes;
    private GameObject elem;
    private GameObject zonavida;
    private GameObject popupr3;
    private GameObject PantallaInicioReto3;
    private GameObject fondonegro;
    private GameObject ayuda;
    private GameObject trofeo;
    private GameObject corazon;
    private GameObject pantallafuturista;
    private GameObject Paneltarjeta;
    private GameObject leyenda1;

    private GameObject leyenda2;
    private GameObject leyenda3;
    private GameObject leyenda4;
    private GameObject leyenda5;
    private GameObject leyenda6;
    private GameObject leyenda7;
    private GameObject leyenda8;
    private GameObject leyenda1mini;

    private GameObject leyenda2mini;
    private GameObject leyenda3mini;
    private GameObject leyenda4mini;
    private GameObject leyenda5mini;
    private GameObject leyenda6mini;
    private GameObject leyenda7mini;
    private GameObject leyenda8mini;

	private GameObject leyenda1mini2;

	private GameObject leyenda2mini2;
	private GameObject leyenda3mini2;
	private GameObject leyenda4mini2;
	private GameObject leyenda5mini2;
	private GameObject leyenda6mini2;
	private GameObject leyenda7mini2;
	private GameObject leyenda8mini2;

    private SoundManager soundManager;


    // Use this for initialization
    private void Start()
    {
        Reto3Obj = transform.FindChild("Reto3").gameObject;
        Reto3Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;

        Reto4Obj = transform.FindChild("Reto4").gameObject;
        fondo = Reto3Obj.transform.GetChild(0).FindChild("fondo").gameObject;

        popupr3 = fondo.transform.FindChild("popupr3").gameObject;
        btnaceptar3 = popupr3.transform.FindChild("btnaceptar3").GetComponent<Button>();

        PantallaInicioReto3 = Reto3Obj.transform.GetChild(0).FindChild("PantallaInicioReto3").gameObject;
        pantallafuturista = PantallaInicioReto3.transform.FindChild("pantallafuturista").gameObject;
        btnlupa = pantallafuturista.transform.FindChild("btnlupa").GetComponent<Button>();
        btnflechaizq = PantallaInicioReto3.transform.FindChild("btnflechaizq").GetComponent<Button>();
        btnflechader = PantallaInicioReto3.transform.FindChild("btnflechader").GetComponent<Button>();
        fondonegro = PantallaInicioReto3.transform.FindChild("fondonegro").gameObject;
        ayuda = PantallaInicioReto3.transform.FindChild("ayuda").gameObject;
        btnaceptarayu = ayuda.transform.FindChild("btnaceptar").GetComponent<Button>();
        zonanaturaleza = PantallaInicioReto3.transform.FindChild("zonanaturaleza").gameObject;
        zonalocalidad = PantallaInicioReto3.transform.FindChild("zonalocalidad").gameObject;
        zonaheroes = PantallaInicioReto3.transform.FindChild("zonaheroes").gameObject;
        zonavida = PantallaInicioReto3.transform.FindChild("zonavida").gameObject;
        corazon = PantallaInicioReto3.transform.FindChild("corazon").gameObject;
        
        popupexito = PantallaInicioReto3.transform.FindChild("popupexito").gameObject;
        btnexitoo = popupexito.transform.FindChild("btnaceptarexito").GetComponent<Button>();
        brillo_trofeo = PantallaInicioReto3.transform.FindChild("brillo-trofeo").gameObject;
        trofeo = PantallaInicioReto3.transform.FindChild("trofeo").gameObject;
        fondotrofeo = PantallaInicioReto3.transform.FindChild("fondotrofeo").gameObject;
        popuppierdevidas = PantallaInicioReto3.transform.FindChild("popuppierdevidas").gameObject;
        btnaceptarpierde = popuppierdevidas.transform.FindChild("btnaceptarpierdee").GetComponent<Button>();
        popupintentar = PantallaInicioReto3.transform.FindChild("popupintentar").gameObject;
        btnaceptarintentar = popupintentar.transform.FindChild("btnaceptarintentar").GetComponent<Button>();
        btnreto4 = popupintentar.transform.FindChild("btnreto4").GetComponent<Button>();
        soundManager = transform.GetComponent<SoundManager>();
        soundManager.playSound("Sound_SonidoFondo2", 0);


		       
        leyenda1 = PantallaInicioReto3.transform.FindChild("leyenda1").gameObject;
		leyenda2 = PantallaInicioReto3.transform.FindChild("leyenda2").gameObject;
        leyenda3 = PantallaInicioReto3.transform.FindChild("leyenda3").gameObject;
        leyenda4 = PantallaInicioReto3.transform.FindChild("leyenda4").gameObject;
        leyenda5 = PantallaInicioReto3.transform.FindChild("leyenda5").gameObject;
        leyenda6 = PantallaInicioReto3.transform.FindChild("leyenda6").gameObject;
        leyenda7 = PantallaInicioReto3.transform.FindChild("leyenda7").gameObject;
        leyenda8 = PantallaInicioReto3.transform.FindChild("leyenda8").gameObject;
        leyenda1mini = PantallaInicioReto3.transform.FindChild("leyenda1mini").gameObject;
		//leyenda1mini2 = PantallaInicioReto3.transform.FindChild("leyenda1mini2").gameObject;
        leyenda2mini = PantallaInicioReto3.transform.FindChild("leyenda2mini").gameObject;
        leyenda3mini = PantallaInicioReto3.transform.FindChild("leyenda3mini").gameObject;
        leyenda4mini = PantallaInicioReto3.transform.FindChild("leyenda4mini").gameObject;
        leyenda5mini = PantallaInicioReto3.transform.FindChild("leyenda5mini").gameObject;
        leyenda6mini = PantallaInicioReto3.transform.FindChild("leyenda6mini").gameObject;
        leyenda7mini = PantallaInicioReto3.transform.FindChild("leyenda7mini").gameObject;
        leyenda8mini = PantallaInicioReto3.transform.FindChild("leyenda8mini").gameObject;
		leyenda1mini2 = PantallaInicioReto3.transform.FindChild("leyenda1mini2").gameObject;
		//leyenda1mini2 = PantallaInicioReto3.transform.FindChild("leyenda1mini2").gameObject;
		leyenda2mini2 = PantallaInicioReto3.transform.FindChild("leyenda2mini2").gameObject;
		leyenda3mini2 = PantallaInicioReto3.transform.FindChild("leyenda3mini2").gameObject;
		leyenda4mini2 = PantallaInicioReto3.transform.FindChild("leyenda4mini2").gameObject;
		leyenda5mini2 = PantallaInicioReto3.transform.FindChild("leyenda5mini2").gameObject;
		leyenda6mini2 = PantallaInicioReto3.transform.FindChild("leyenda6mini2").gameObject;
		leyenda7mini2 = PantallaInicioReto3.transform.FindChild("leyenda7mini2").gameObject;
		leyenda8mini2 = PantallaInicioReto3.transform.FindChild("leyenda8mini2").gameObject;



        Invoke("inicioreto", 2f);

        btnaceptar3.onClick.AddListener(delegate
        {
            popupr3.SetActive(false);
            PantallaInicioReto3.SetActive(true);
           	opcionleyenda = Random.Range(1,6);
			corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
			
			mostrarleyenda();

        });

        btnaceptarintentar.onClick.AddListener(delegate
        {
            //reiniciar marcador y reto

            popupintentar.SetActive(false);
            contadorleyenda = 0;
            PantallaInicioReto3.SetActive(true);
            //leyenda1.SetActive(true);
            //btnflechader.gameObject.SetActive(true);
            ley = 0;
            //numcorazones = 0;
            contadorleyenda = 0;
			
			corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
			intentar=true;
            leyenda1.SetActive(false);
            leyenda2.SetActive(false);
            leyenda3.SetActive(false);
            leyenda4.SetActive(false);
            leyenda5.SetActive(false);
            leyenda6.SetActive(false);
            leyenda7.SetActive(false);
            leyenda8.SetActive(false);
            leyenda1mini.SetActive(false);
            leyenda2mini.SetActive(false);
            leyenda3mini.SetActive(false);
            leyenda4mini.SetActive(false);
            leyenda5mini.SetActive(false);
            leyenda6mini.SetActive(false);
            leyenda7mini.SetActive(false);
            leyenda8mini.SetActive(false);
				leyenda1mini2.SetActive(false);
				leyenda2mini2.SetActive(false);
				leyenda3mini2.SetActive(false);
				leyenda4mini2.SetActive(false);
				leyenda5mini2.SetActive(false);
				leyenda6mini2.SetActive(false);
				leyenda7mini2.SetActive(false);
				leyenda8mini2.SetActive(false);

            pantallafuturista.SetActive(true);
            btnlupa.gameObject.SetActive(true);
            btnflechaizq.gameObject.SetActive(false);
            btnflechader.gameObject.SetActive(false);
            fondonegro.gameObject.SetActive(false);


            brillo_trofeo.SetActive(false);
            trofeo.SetActive(false);
            fondotrofeo.SetActive(false);
            reinicio = true;
            inicioreto();
			
        });

        btnaceptarpierde.onClick.AddListener(delegate
        {

            //reiniciar marcador y reto
            PlayerPrefs.SetInt("corazonestest", 0);
            //soundManager.playSound("Sound_correcto10", 1);
            //Invoke("Revisarpuntuacion", 0f);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
            reinicio = true;
             popuppierdevidas.SetActive(false);
             contadorleyenda = 0;
             PantallaInicioReto3.SetActive(true);
             //leyenda1.SetActive(true);
			 //numcorazones=0;
             //btnflechader.gameObject.SetActive(true);
             ley = 0;
             
             //numcorazones = 0;
             contadorleyenda = 0;
           // popuppierdevidas
             //numcorazones=PlayerPrefs.GetInt("corazonestest");

            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
             reinicio = true;

             leyenda1.SetActive(false);
             leyenda2.SetActive(false);
             leyenda3.SetActive(false);
             leyenda4.SetActive(false);
             leyenda5.SetActive(false);
             leyenda6.SetActive(false);
             leyenda7.SetActive(false);
             leyenda8.SetActive(false);
             leyenda1mini.SetActive(false);
             leyenda2mini.SetActive(false);
             leyenda3mini.SetActive(false);
             leyenda4mini.SetActive(false);
             leyenda5mini.SetActive(false);
             leyenda6mini.SetActive(false);
             leyenda7mini.SetActive(false);
             leyenda8mini.SetActive(false);
			 leyenda1mini2.SetActive(false);
			 leyenda2mini2.SetActive(false);
			 leyenda3mini2.SetActive(false);
			 leyenda4mini2.SetActive(false);
			 leyenda5mini2.SetActive(false);
			 leyenda6mini2.SetActive(false);
			 leyenda7mini2.SetActive(false);
			 leyenda8mini2.SetActive(false);

             pantallafuturista.SetActive(true);
             btnlupa.gameObject.SetActive(true);
             btnflechaizq.gameObject.SetActive(false);
             btnflechader.gameObject.SetActive(false);
             fondonegro.gameObject.SetActive(false);


             brillo_trofeo.SetActive(false);
             trofeo.SetActive(false);
             fondotrofeo.SetActive(false);
             inicioreto();
        });


        btnlupa.onClick.AddListener(delegate
        {
            fondonegro.SetActive(true);
            ayuda.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp10", 1);
        });

        btnaceptarayu.onClick.AddListener(delegate
        {
            fondonegro.SetActive(false);
            ayuda.SetActive(false);
        });

        btnexitoo.onClick.AddListener(delegate
        {
            popupintentar.SetActive(false);
            popupexito.SetActive(false);
            pantallafuturista.SetActive(false);
            soundManager.playSound("Sound_FuegosArtificiales", 1);
            activartrofeo();
			
        });

        btnreto4.onClick.AddListener(delegate
        {
            popupintentar.SetActive(false);
			//PlayerPrefs.SetInt("test",numcorazones);
            Invoke("reto4", 0f);
        });


        btnflechader.onClick.AddListener(delegate
        {
            if (ley == 1)
            {
                leyenda1.SetActive(false);
                leyenda2.SetActive(true);
                ley = 2;
                btnflechaizq.gameObject.SetActive(true);
                btnflechader.gameObject.SetActive(true);
            }
            else
            {
                if (ley == 2)
                {
                    leyenda1.SetActive(false);
                    leyenda2.SetActive(false);
                    leyenda3.SetActive(true);
                    btnflechader.gameObject.SetActive(true);
                    ley = 3;
                }
                else
                {
                    if (ley == 3)
                    {
                        leyenda1.SetActive(false);
                        leyenda2.SetActive(false);
                        leyenda3.SetActive(false);
                        leyenda4.SetActive(true);
                        btnflechader.gameObject.SetActive(true);
                        ley = 4;
                    }
                    else
                    {
                        if (ley == 4)
                        {
                            leyenda1.SetActive(false);
                            leyenda2.SetActive(false);
                            leyenda3.SetActive(false);
                            leyenda4.SetActive(false);
                            leyenda5.SetActive(true);
                            btnflechader.gameObject.SetActive(true);
                            ley = 5;
                        }
                        else
                        {
                            if (ley == 5)
                            {
                                leyenda1.SetActive(false);
                                leyenda2.SetActive(false);
                                leyenda3.SetActive(false);
                                leyenda4.SetActive(false);
                                leyenda5.SetActive(false);
                                leyenda6.SetActive(true);
                                ley = 6;
                                btnflechader.gameObject.SetActive(true);
                            }
                            else
                            {
                                if (ley == 6)
                                {
                                    leyenda1.SetActive(false);
                                    leyenda2.SetActive(false);
                                    leyenda3.SetActive(false);
                                    leyenda4.SetActive(false);
                                    leyenda5.SetActive(false);
                                    leyenda6.SetActive(false);
                                    leyenda7.SetActive(true);
                                    ley = 7;
                                    btnflechader.gameObject.SetActive(true);
                                }
                               
                            }
                        }
                    }
                }
            } //fin de else1
        });


        btnflechaizq.onClick.AddListener(delegate
        {
          
                if (ley == 3)
                {
                    leyenda1.SetActive(false);
                    leyenda2.SetActive(true);
                    leyenda3.SetActive(false);
                    btnflechader.gameObject.SetActive(true);
                    btnflechaizq.gameObject.SetActive(true);
                    ley = 2;
                }
                else
                {
                    if (ley == 4)
                    {
                        leyenda4.SetActive(false);
                        leyenda3.SetActive(true);
                        btnflechader.gameObject.SetActive(true);
                        btnflechaizq.gameObject.SetActive(true);
                        ley = 3;
                    }
                    else
                    {
                        if (ley == 5)
                        {
                            leyenda4.SetActive(true);
                            leyenda5.SetActive(false);
                            btnflechader.gameObject.SetActive(true);
                            btnflechaizq.gameObject.SetActive(true);

                            ley = 4;
                        }
                        else
                        {
                            if (ley == 6)
                            {
                                leyenda5.SetActive(true);
                                leyenda6.SetActive(false);
                                btnflechader.gameObject.SetActive(true);
                                btnflechaizq.gameObject.SetActive(true);
                                ley = 5;
                            }
                            else
                            {
                                if (ley == 7)
                                {
                                    leyenda6.SetActive(true);
                                    leyenda7.SetActive(false);
                                    btnflechader.gameObject.SetActive(true);
                                    btnflechaizq.gameObject.SetActive(true);

                                    ley = 6;
                                }
                               
                            }
                        }
                    }
                }
            
        });
    }

    // Update is called once per frame
    private void Update()
    {
        if (Reto3Obj.activeInHierarchy && !flag)
        {
            
			corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
            flag = true;
        }

        if (Input.touchCount > 0)
        {
            var pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            var hit = Physics2D.Raycast(pos, Vector2.zero);


            if ((hit.collider != null) && (Input.GetTouch(0).phase == TouchPhase.Began))
            {
                
                print("Startedss");
                Debug.Log(hit.collider.gameObject.name);
                try
                {
                    print(hit.collider.gameObject.name);
                    //pos = hit.collider.gameObject.transform.localPosition;
                    if ((hit.collider.gameObject.name == "leyenda1") || (hit.collider.name == "leyenda2") ||
                        (hit.collider.gameObject.name == "leyenda3") || (hit.collider.gameObject.name == "leyenda4") ||
                        (hit.collider.gameObject.name == "leyenda5") || (hit.collider.gameObject.name == "leyenda6") ||
                        (hit.collider.gameObject.name == "leyenda7") || (hit.collider.gameObject.name == "leyenda8"))
                    {
                       

						elem = hit.collider.gameObject;
						elem.transform.localScale= new Vector3(0.15f,0.15f,1f);

						print("agarra elemento");
						//leyenda1.SetActive(false);
						posIni = hit.collider.gameObject.transform.localPosition;
						dragging = true;


                    }
                }
                catch (Exception ex)
                {
                    Debug.Log(ex.Message);
                }
            }


            if (Input.GetTouch(0).phase == TouchPhase.Moved)
              
			if (dragging)
                {
                    print("Testing" + elem.name);
                    elem.transform.position = new Vector3(pos.x, pos.y + .5f, elem.transform.position.z);
                }


            if (Input.GetTouch(0).phase == TouchPhase.Ended)
				
                if (dragging)
                {
				//int aux = 0;    
				if ((Vector3.Distance(elem.transform.localPosition, zonanaturaleza.transform.localPosition) < 50) &&
                        (elem.name == "leyenda1"))
                    {
                        elem.transform.localPosition = zonanaturaleza.transform.localPosition;
                        leyenda1.SetActive(false);
						//leyenda1mini2.SetActive(false);
                        leyenda1mini.SetActive(true);
                        elem.transform.localPosition = posIni;
						leyenda1mini.SetActive (false);
						leyenda1mini2.SetActive (true);
                        leyenda2.SetActive(true);
                        
                        contadorleyenda++;
						aux=PlayerPrefs.GetInt("corazonestest");
						aux++;
						print ("Auxiliar : " + aux);
						PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                        
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonaheroes.transform.localPosition) < 50) &&
                             (elem.name == "leyenda7"))
                    {
                        elem.transform.localPosition = zonaheroes.transform.localPosition;
                        leyenda7.SetActive(false);
                        leyenda7mini.SetActive(true);
                        elem.transform.localPosition = posIni;
					    leyenda7mini.SetActive(false);
						leyenda7mini2.SetActive (true);
                        leyenda8.SetActive(true);
                        
                        contadorleyenda++;

						aux=PlayerPrefs.GetInt("corazonestest");
						aux++;
						print ("Auxiliar : " + aux);
						PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                       // Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonavida.transform.localPosition) < 50) &&
                             (elem.name == "leyenda8"))
                    {
                        elem.transform.localPosition = zonavida.transform.localPosition;
                        leyenda8.SetActive(false);
                        leyenda8mini.SetActive(true);
                        elem.transform.localPosition = posIni;
					    leyenda8mini.SetActive(false);
						leyenda8mini2.SetActive (true);
                        leyenda1.SetActive(true);
                        
                        contadorleyenda++;

					aux=PlayerPrefs.GetInt("corazonestest");
					aux++;
					print ("Auxiliar : " + aux);
					PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                       // Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonanaturaleza.transform.localPosition) < 50) &&
                             (elem.name == "leyenda2"))
                    {
                        elem.transform.localPosition = zonanaturaleza.transform.localPosition;
                        leyenda2.SetActive(false);
                        leyenda2mini.SetActive(true);
                        elem.transform.localPosition = posIni;
						leyenda2mini.SetActive(false);
						leyenda2mini2.SetActive (true);
                        leyenda3.SetActive(true);
                       
					contadorleyenda++;
					aux=PlayerPrefs.GetInt("corazonestest");
					aux++;
					print ("Auxiliar : " + aux);
					PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                        //Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonalocalidad.transform.localPosition) < 50) &&
                             (elem.name == "leyenda3"))
                    {
                        elem.transform.localPosition = zonalocalidad.transform.localPosition;
                        leyenda3.SetActive(false);
                        leyenda3mini.SetActive(true);
                        elem.transform.localPosition = posIni;
						leyenda3mini.SetActive(false);
						leyenda3mini2.SetActive (true);
                        leyenda4.SetActive(true);
					contadorleyenda++;
					aux=PlayerPrefs.GetInt("corazonestest");
					aux++;
					print ("Auxiliar : " + aux);
					PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                       // Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonanaturaleza.transform.localPosition) < 50) &&
                             (elem.name == "leyenda4"))
                    {
                        elem.transform.localPosition = zonanaturaleza.transform.localPosition;
                        leyenda4.SetActive(false);
                        leyenda4mini.SetActive(true);
                        elem.transform.localPosition = posIni;
						leyenda4mini.SetActive(false);
						leyenda4mini2.SetActive (true);
                        leyenda5.SetActive(true);
					contadorleyenda++;
					aux=PlayerPrefs.GetInt("corazonestest");
					aux++;
					print ("Auxiliar : " + aux);
					PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                        //Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonavida.transform.localPosition) < 50) &&
                             (elem.name == "leyenda5"))
                    {
                        elem.transform.localPosition = zonavida.transform.localPosition;
                        leyenda5.SetActive(false);
                        leyenda5mini.SetActive(true);
                        elem.transform.localPosition = posIni;
						leyenda5mini.SetActive(false);
						leyenda5mini2.SetActive (true);
                        leyenda6.SetActive(true);
					contadorleyenda++;
					aux=PlayerPrefs.GetInt("corazonestest");
					aux++;
					print ("Auxiliar : " + aux);
					PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                        //Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, zonaheroes.transform.localPosition) < 50) &&
                             (elem.name == "leyenda6"))
                    {
                        elem.transform.localPosition = zonaheroes.transform.localPosition;
                        leyenda6.SetActive(false);
                        leyenda6mini.SetActive(true);
						
                        elem.transform.localPosition = posIni;
						leyenda6mini.SetActive(false);
						leyenda6mini2.SetActive (true);
                        leyenda7.SetActive(true);
					contadorleyenda++;
					aux=PlayerPrefs.GetInt("corazonestest");
					aux++;
					print ("Auxiliar : " + aux);
					PlayerPrefs.SetInt("corazonestest",aux);
                        soundManager.playSound("Sound_correcto10", 1);
                        //Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
                        Invoke("Revisarpuntuacion", 0f);
                    }

                    else
                    {
						
						elem.transform.localPosition = posIni;
                        elem.SetActive(true);
					    elem.transform.localScale= new Vector3(1f,1f,1f);
						aux=PlayerPrefs.GetInt("corazonestest");
						aux--;
						PlayerPrefs.SetInt("corazonestest",aux);

                        soundManager.playSound("Sound_incorrecto19", 1);
                        Invoke("Revisarpuntuacion", 0f);
					corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();

                    }
				    //poner playerprefs
				    
                    //print(contadorleyenda);


                   
				    elem.transform.localScale= new Vector3(1f,1f,1f);

                    //revisar los corazones
				if ((PlayerPrefs.GetInt("corazonestest") >= 10) && (contadorleyenda == 6))
                    {
                        popupexito.SetActive(true);
                        soundManager.playSound("Sound_GanarVida11", 1);
                        gana = true;
                        soundManager.playSound("Sound_FuegosArtificiales", 1);

                    }

				if ((contadorleyenda == 6) && (PlayerPrefs.GetInt("corazonestest")  < 10) && (gana == false))
                    {
                        popupintentar.SetActive(true);
                        soundManager.playSound("Sound_OpenPopUp10", 1);

                    }


                    dragging = false;
                } //fin de draggin

        } //fin de input touch > 0
    }


    private void reto4()
    {
        Reto3Obj.SetActive(false);
        Reto4Obj.SetActive(true);
        soundManager.playSound("Fondo_piano", 0);
		//PlayerPrefs.SetInt("test",numcorazones);
    }

    private void Revisarpuntuacion()
    {
        //if (PlayerPrefs.GetInt("corazonestest")  <= 0  && intentar==false)
        if (PlayerPrefs.GetInt("corazonestest") <= 0 )
        {
			//if (intentar == false) {
				
				soundManager.playSound ("Sound_PerderVida10", 1);
            PlayerPrefs.SetInt("corazonestest", 0);
           
            //soundManager.playSound("Sound_correcto10", 1);
            //Invoke("Revisarpuntuacion", 0f);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
            popuppierdevidas.SetActive(true);
            leyenda1.SetActive (false);
				leyenda2.SetActive (false);
				leyenda3.SetActive (false);
				leyenda4.SetActive (false);
				leyenda5.SetActive (false);
				leyenda6.SetActive (false);
				leyenda7.SetActive (false);
				leyenda8.SetActive (false);
			//} 

        }

		PlayerPrefs.GetInt("corazonestest");
		aux=PlayerPrefs.GetInt("corazonestest");
		print(aux);
		//PlayerPrefs.SetInt ("corazonesODA2_ODA2", numcorazones);
		PlayerPrefs.SetInt("corazonestest", aux);

       


        
    }

    private void inicioreto()
    {
		

		if (reinicio == false)
        { 
			
			popupr3.SetActive(true);
			//corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("test").ToString();
            soundManager.playSound("Sound_OpenPopUp10", 1);
			//numcorazones=PlayerPrefs.GetInt("corazonesODA2_ODA2");



        }
		corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
			PlayerPrefs.GetInt("corazonestest").ToString();
        ley = 0;
        //numcorazones = 4;
        gana = false;

        if (reinicio ==true)
        { 
        PantallaInicioReto3.SetActive(true);
        //leyenda1.SetActive(true);
        //ley = 1;
        opcionleyenda = Random.Range(1, 6);
        mostrarleyenda();
		//numcorazones = 0;
        }


    }

    private void activartrofeo()
    {
        soundManager.playSound("Sound_FuegosArtificiales", 1);
        fondotrofeo.SetActive(true);
        brillo_trofeo.SetActive(true);
        trofeo.SetActive(true);
        Invoke("reto4", 12f);
    }


	private void mostrarleyenda(){
		
		if (opcionleyenda == 1) {
			
			leyenda2.SetActive (true);
			leyenda1.SetActive (false);
			leyenda3.SetActive (false);
			leyenda4.SetActive (false);
			leyenda5.SetActive (false);
			leyenda6.SetActive (false);
			leyenda7.SetActive (false);
			leyenda8.SetActive (false);
			btnflechader.gameObject.SetActive(true);
			btnflechaizq.gameObject.SetActive(false);
			ley = 2;

		}
		if (opcionleyenda == 2) {
			leyenda3.SetActive (true);
			leyenda1.SetActive (false);
			leyenda2.SetActive (false);
			leyenda4.SetActive (false);
			leyenda5.SetActive (false);
			leyenda6.SetActive (false);
			leyenda7.SetActive (false);
			leyenda8.SetActive (false);
			btnflechader.gameObject.SetActive(true);
			btnflechaizq.gameObject.SetActive(true);
			ley = 3;


		}
		if (opcionleyenda == 3) {
			
			leyenda4.SetActive (true);
			leyenda1.SetActive (false);
			leyenda2.SetActive (false);
			leyenda3.SetActive (false);
			leyenda5.SetActive (false);
			leyenda6.SetActive (false);
			leyenda7.SetActive (false);
			leyenda8.SetActive (false);
			btnflechader.gameObject.SetActive(true);
			btnflechaizq.gameObject.SetActive(true);

			ley = 4;

		}
		if (opcionleyenda == 4) {
			
			leyenda5.SetActive (true);
			leyenda1.SetActive (false);
			leyenda2.SetActive (false);
			leyenda3.SetActive (false);
			leyenda4.SetActive (false);
			leyenda6.SetActive (false);
			leyenda7.SetActive (false);
			leyenda8.SetActive (false);
			btnflechader.gameObject.SetActive(true);
			btnflechaizq.gameObject.SetActive(true);
			ley = 5;

		}
		if (opcionleyenda == 5) {
			leyenda6.SetActive (true);
			leyenda1.SetActive (false);
			leyenda2.SetActive (false);
			leyenda3.SetActive (false);
			leyenda4.SetActive (false);
			leyenda5.SetActive (false);
			leyenda7.SetActive (false);
			leyenda8.SetActive (false);
			btnflechader.gameObject.SetActive(true);
			btnflechaizq.gameObject.SetActive(true);
			ley = 6;
		
		}
		
		if (opcionleyenda == 6) {
			leyenda7.SetActive (true);
			leyenda1.SetActive (false);
			leyenda2.SetActive (false);
			leyenda3.SetActive (false);
			leyenda4.SetActive (false);
			leyenda5.SetActive (false);
			leyenda6.SetActive (false);
			leyenda8.SetActive (false);
			btnflechader.gameObject.SetActive(true);
			btnflechaizq.gameObject.SetActive(true);

			ley = 7;


		}

	
	}
}