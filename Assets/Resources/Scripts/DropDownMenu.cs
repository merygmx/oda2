﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DropDownMenu : MonoBehaviour
{

	GameObject  Index, Portada, RetoFinal, Reto1Obj, Reto2Obj, Reto3Obj, Reto4Obj;
	public GameObject Clon;

	//Sonidos
	SoundManager soundManager;

	//Prebaf
	GameObject dropDownMenuGameObject;
	Boolean openMenu;

	//Animacion
	private Animator menuAnimator;

	//Botones
	Button toggleMenuButton, closePopUpButton;


	//private GameObject popUpContainterGameObject;
	GameObject imgFichaTecnicaGameObject, imgCreditosGameObject, imgGlosarioGameObject;

	Button fichaTecnicaButton, creditosButton, glosarioButton, inicioButton;

	// Use this for initialization
	void Start () {

		Index = this.transform.GetChild (0).gameObject;
		Portada = this.transform.GetChild (1).gameObject;
		Reto1Obj = this.transform.FindChild ("Reto1").gameObject;
		Reto2Obj= this.transform.FindChild ("Reto2").gameObject;
		Reto3Obj= this.transform.FindChild ("Reto3").gameObject;
		Reto4Obj= this.transform.FindChild ("Reto4").gameObject;
		RetoFinal= this.transform.FindChild ("RetoFinal").gameObject;

		//Sonidos
		soundManager = transform.GetComponent<SoundManager>();

		dropDownMenuGameObject = this.transform.FindChild("dropDownMenu").gameObject;
		dropDownMenuGameObject.transform.GetComponent<Canvas>().worldCamera = Camera.main;
		toggleMenuButton = dropDownMenuGameObject.transform.GetChild(0).GetComponent<Button>();
		menuAnimator = dropDownMenuGameObject.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
		closePopUpButton = dropDownMenuGameObject.transform.GetChild(1).GetComponent<Button>();
		imgFichaTecnicaGameObject = closePopUpButton.transform.GetChild(0).GetChild(0).gameObject;
		imgCreditosGameObject = closePopUpButton.transform.GetChild(0).GetChild(1).gameObject;
		imgGlosarioGameObject = closePopUpButton.transform.GetChild(0).GetChild(2).gameObject;
		fichaTecnicaButton = menuAnimator.transform.GetChild(0).GetComponent<Button>();
		creditosButton = menuAnimator.transform.GetChild(1).GetComponent<Button>();
		glosarioButton = menuAnimator.transform.GetChild(2).GetComponent<Button>();
		inicioButton = menuAnimator.transform.GetChild(3).GetComponent<Button>();
		toggleMenuButton.onClick.AddListener(ToggleMenu);
		closePopUpButton.onClick.AddListener(ClosePopUps);

		fichaTecnicaButton.onClick.AddListener(delegate () {
			OpenPopUp("Ficha_Tecnica");
		});
		creditosButton.onClick.AddListener(delegate () {
			OpenPopUp("Creditos");
		});
		glosarioButton.onClick.AddListener(delegate () {
			OpenPopUp("Glosario");
		});
		inicioButton.onClick.AddListener(delegate () {
			GoToInicio();

		});


	}

	void Update(){

		if (Index.activeInHierarchy || RetoFinal.activeInHierarchy) {
			//inicioButton.gameObject.SetActive (false);
			toggleMenuButton.gameObject.SetActive(false);
		}
		else{
			//inicioButton.gameObject.SetActive (false);
			toggleMenuButton.gameObject.SetActive(true);
		}


		if (Portada.activeInHierarchy) {
			dropDownMenuGameObject.SetActive (true);
            toggleMenuButton.gameObject.SetActive(true);
            inicioButton.gameObject.SetActive (false);

		} else {
			inicioButton.gameObject.SetActive (true);


		}

	}

	private void GoToInicio()
	{
		irMenu ();
		Portada.SetActive (true); 
		ToggleMenu();

        
		//this.GetComponent<Reto1Oda2>().Clon.SetActive(true);
        //Destroy(this.gameObject);*/
		Destroy(this.gameObject);
		this.GetComponent<Reto1Oda2> ().Clon.name = "ODA2";
		this.GetComponent<Reto1Oda2>().Clon.SetActive(true);
		//Clon.name="ODA2";
		//Clon.SetActive (true);
		dropDownMenuGameObject.SetActive (true);
    }

	private void OpenPopUp(string popUpName)
	{

		closePopUpButton.gameObject.SetActive(true);
		if (popUpName == "Ficha_Tecnica")
		{
			imgFichaTecnicaGameObject.SetActive(true);
		}
		else if (popUpName == "Creditos")
		{
			imgCreditosGameObject.SetActive(true);
		}
		else if (popUpName == "Glosario")
		{
			imgGlosarioGameObject.SetActive(true);
		}



	}


	void ClosePopUps()
	{
		imgFichaTecnicaGameObject.SetActive(false);
		imgCreditosGameObject.SetActive(false);
		imgGlosarioGameObject.SetActive(false);
		closePopUpButton.gameObject.SetActive(false);
	}


	void ToggleMenu()
	{ 
		openMenu = !openMenu;
		menuAnimator.SetBool("Opened", openMenu);
	}

	public void irMenu()
	{

		Portada.SetActive (true); 
		Index.SetActive (false);
		Reto1Obj.SetActive (false);
		Reto2Obj.SetActive (false);
		Reto3Obj.SetActive (false);
		Reto4Obj.SetActive (false);
		RetoFinal.SetActive (false);

		//menuScript.abrirMenu();
		soundManager.playSound("Fondo_piano", 0);        
	}

}
