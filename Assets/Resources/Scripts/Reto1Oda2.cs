﻿using UnityEngine;
using UnityEngine.UI;

public class Reto1Oda2 : MonoBehaviour
{
    private bool animationStarted;
    private int bandera;
    private int bandera2;
    private int banderaleyenda;
    private int rand;
    private int rand2;
    private int numcorazones = 2;
    private int corazonesreto;
    private Button btnnom3l2;
    private Button btnaceptarm3l2;
    private Button btnaceptarpierdevidas;
    private Button btnaceptarfinal;
    private Button btnnom3l3;
    private Button btnaceptarm3l3;
    private Button btnnom3l4;
    private Button btnaceptarm3l4;
    private Button btnnom3l5;
    private Button btnaceptarm3l5;
    private Button btnentrar;
    private Button btnReto1;
    private Button btnReto2;
    private Button btnReto3;
    private Button btnReto4;
    private Button btnRetoFinal;
    private Button btnaceptar;
    private Button btnfogata;
    private Button btnaceptarp3;
    private Button btnAceptar;
    private Button btnsig;
    private Button btnant;
    private Button btnMision1;
    private Button btnMision2;
    private Button btnMision3;
    private Button btnAceptarl2;
    private Button btnsigl2;
    private Button btnantl2;
    private Button btnAceptarl3;
    private Button btnsigl3;
    private Button btnantl3;
    private Button btnAceptarl4;
    private Button btnsigl4;
    private Button btnantl4;
    private Button btnAceptarl5;
    private Button btnsigl5;
    private Button btnantl5;
    private Button btnnom1l1;
    private Button btnaceptarm1l1;
    private Button btnnom2l1;
    private Button btnaceptarm2l1;
    private Button btnnom3l1;
    private Button btnaceptarm3l1;
    private Button btnnom1l2;
    private Button btnsim1l2;
    private Button btnnom1l3;
    private Button btnsim1l3;
    private Button btnnom1l4;
    private Button btnsim1l4;
    private Button btnnom1l5;
    private Button btnsim1l5;
    private Button btnnom2l2;
    private Button btnaceptarm2l2;
    private Button btnnom2l3;
    private Button btnaceptarm2l3;
    private Button btnnom2l4;
    private Button btnaceptarm2l4;
    private Button btnnom2l5;
    private Button btnaceptarm2l5;
    public GameObject Clon;
    private bool flag;
    private readonly bool reinicio = false;
    private bool dragging = false;
    private bool mostrarley;
    private bool m = false;
    private bool banderamision;
    private bool m1;
    private bool m2;
    private bool m3;
	private bool m1a;
	private bool m2a;
	private bool m3a;
    private GameObject Index;
    private GameObject menu;
    private GameObject popuppierdevidas;
    private GameObject corazon;
    private GameObject Mision3OpcionLeyenda2;
    private GameObject fondonegrom3l2;
    private GameObject popupm3l2;
    private GameObject ancianom3l2;
    private GameObject globoexitom3l2;
    private GameObject globoerrorm3l2;
    private GameObject Mision3OpcionLeyenda3;
    private GameObject fondonegrom3l3;
    private GameObject popupm3l3;
    private GameObject ancianom3l3;
    private GameObject globoexitom3l3;
    private GameObject globoerrorm3l3;
    private GameObject Mision3OpcionLeyenda4;
    private GameObject fondonegrom3l4;
    private GameObject popupm3l4;
    private GameObject ancianom3l4;
    private GameObject globoexitom3l4;
    private GameObject globoerrorm3l4;
    private GameObject Mision3OpcionLeyenda5;
    private GameObject fondonegrom3l5;
    private GameObject popupm3l5;
    private GameObject ancianom3l5;
    private GameObject globoexitom3l5;
    private GameObject globoerrorm3l5;
    private GameObject globoerrorm2l3;
    private GameObject Portada;
    private GameObject Reto1Obj;
    private GameObject Reto2Obj;
    private GameObject Reto3Obj;
    private GameObject Reto4Obj;
    private GameObject RetoFinalObj;
    private GameObject Pantalla2;
    private GameObject popupp2;
    private GameObject Pantalla3;
    private GameObject fondonegro;
    private GameObject fondonegro2;
    private GameObject popupp3;
    private GameObject Leyenda1;
    private GameObject Panel;
    private GameObject tarjeta1l1;
    private GameObject tarjeta2l1;
    private GameObject tarjeta3l1;
    private GameObject tarjeta4l1;
    private GameObject Pantalla4;
    private GameObject Leyenda2;
    private GameObject Panell2;
    private GameObject tarjeta1l2;
    private GameObject tarjeta2l2;
    private GameObject Leyenda3;
    private GameObject Panell3;
    private GameObject tarjeta1l3;
    private GameObject tarjeta2l3;
    private GameObject tarjeta3l3;
    private GameObject Leyenda4;
    private GameObject Panell4;
    private GameObject tarjeta1l4;
    private GameObject tarjeta2l4;
    private GameObject tarjeta3l4;
    private GameObject Leyenda5;
    private GameObject Panell5;
    private GameObject tarjeta1l5;
    private GameObject tarjeta2l5;
    private GameObject Mision1OpcionLeyenda1;
    private GameObject fondonegrom1l1;
    private GameObject ancianom1l1;
    private GameObject globoexitom1l1;
    private GameObject globoerrorm1l1;
    private GameObject popupm1l1;
    private GameObject Mision2OpcionLeyenda1;
    private GameObject fondonegrom2l1;
    private GameObject ancianom2l1;
    private GameObject globoexitom2l1;
    private GameObject globoerrorm2l1;
    private GameObject popupm2l1;
    private GameObject Mision3OpcionLeyenda1;
    private GameObject fondonegrom3l1;
    private GameObject ancianom3l1;
    private GameObject globoerrorm3l1;
    private GameObject globoexitom3l1;
    private GameObject popupm3l1;
    private GameObject popupfinalm3l1;
    private GameObject Mision1OpcionLeyenda2;
    private GameObject ancianom1l2;
    private GameObject globoerrorm1l2;
    private GameObject globoexitom1l2;
    private GameObject popupm1l2;
    private GameObject fondonegrom1l2;
    private GameObject Mision1OpcionLeyenda3;
    private GameObject ancianom1l3;
    private GameObject globoerrorm1l3;
    private GameObject globoexitom1l3;
    private GameObject popupm1l3;
    private GameObject fondonegrom1l3;
    private GameObject Mision1OpcionLeyenda4;
    private GameObject ancianom1l4;
    private GameObject globoerrorm1l4;
    private GameObject globoexitom1l4;
    private GameObject popupm1l4;
    private GameObject fondonegrom1l4;
    private GameObject Mision1OpcionLeyenda5;
    private GameObject ancianom1l5;
    private GameObject globoerrorm1l5;
    private GameObject globoexitom1l5;
    private GameObject popupm1l5;
    private GameObject fondonegrom1l5;
    private GameObject Mision2OpcionLeyenda2;
    private GameObject Mision2OpcionLeyenda3;
    private GameObject Mision2OpcionLeyenda4;
    private GameObject Mision2OpcionLeyenda5;
    private GameObject fondonegrom2l2;
    private GameObject fondonegrom2l3;
    private GameObject fondonegrom2l4;
    private GameObject fondonegrom2l5;
    private GameObject popupm2l2;
    private GameObject popupm2l3;
    private GameObject popupm2l4;
    private GameObject popupm2l5;
    private GameObject ancianom2l2;
    private GameObject ancianom2l3;
    private GameObject ancianom2l4;
    private GameObject ancianom2l5;
    private GameObject globoexitom2l2;
    private GameObject globoerrorm2l2;
    private GameObject globoexitom2l3;
    private GameObject globoexitom2l4;
    private GameObject globoerrorm2l4;
    private GameObject globoexitom2l5;
    private GameObject globoerrorm2l5;
    private GameObject popupfinal;
    private Text label;
    private Vector3 posIni;

    private SoundManager soundManager;

    //declare variables

    private string text;
    private float timeAnimation;
    private float timeTotal;


    // Use this for initialization
    private void Start()
    {
        Index = transform.GetChild(0).gameObject;
        Portada = transform.GetChild(1).gameObject;
        btnentrar = Index.transform.GetChild(0).FindChild("btnentrar").GetComponent<Button>();
        btnReto1 = Portada.transform.GetChild(0).FindChild("btnReto1").GetComponent<Button>();
        btnReto2 = Portada.transform.GetChild(0).FindChild("btnReto2").GetComponent<Button>();
        btnReto3 = Portada.transform.GetChild(0).FindChild("btnReto3").GetComponent<Button>();
        btnReto4 = Portada.transform.GetChild(0).FindChild("btnReto4").GetComponent<Button>();
        btnRetoFinal = Portada.transform.GetChild(0).FindChild("btnRetoFinal").GetComponent<Button>();

        menu = transform.FindChild("dropDownMenu").gameObject;

        soundManager = transform.GetComponent<SoundManager>();


        //0 es para musica de fondo 
        //1 todos los sonidos encima de la musica, popups aciertos, errores
        Reto1Obj = transform.FindChild("Reto1").gameObject;
        Reto1Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
        Pantalla2 = Reto1Obj.transform.GetChild(0).FindChild("Pantalla2").gameObject;
        popupp2 = Pantalla2.transform.FindChild("popupp2").gameObject;
        btnaceptar = popupp2.transform.FindChild("btnaceptar").GetComponent<Button>();
        fondonegro = Pantalla2.transform.FindChild("fondonegro").gameObject;
        Pantalla3 = Reto1Obj.transform.GetChild(0).FindChild("Pantalla3").gameObject;
        btnfogata = Pantalla3.transform.FindChild("fogata").GetComponent<Button>();
        fondonegro2 = Pantalla3.transform.FindChild("fondonegro").gameObject;
        popupp3 = Pantalla3.transform.FindChild("popupp3").gameObject;
        btnaceptarp3 = popupp3.transform.FindChild("btnaceptar").GetComponent<Button>();
        
        corazon = Pantalla3.transform.FindChild("corazon").gameObject;


        Leyenda1 = Pantalla3.transform.FindChild("leyenda1").gameObject;
        Panel = Leyenda1.transform.FindChild("Panel").gameObject;
        tarjeta1l1 = Panel.transform.FindChild("tarjeta1").gameObject;
        tarjeta2l1 = Panel.transform.FindChild("tarjeta2").gameObject;
        tarjeta3l1 = Panel.transform.FindChild("tarjeta3").gameObject;
        tarjeta4l1 = Panel.transform.FindChild("tarjeta4").gameObject;
        btnAceptar = tarjeta4l1.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnsig = Panel.transform.FindChild("btnsig").GetComponent<Button>();
        btnant = Panel.transform.FindChild("btnant").GetComponent<Button>();

        Leyenda2 = Pantalla3.transform.FindChild("leyenda2").gameObject;
        Panell2 = Leyenda2.transform.FindChild("Panel").gameObject;
        tarjeta1l2 = Panell2.transform.FindChild("tarjeta1").gameObject;
        tarjeta2l2 = Panell2.transform.FindChild("tarjeta2").gameObject;
        btnAceptarl2 = tarjeta2l2.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnsigl2 = Panell2.transform.FindChild("btnsig").GetComponent<Button>();
        btnantl2 = Panell2.transform.FindChild("btnant").GetComponent<Button>();

        Leyenda3 = Pantalla3.transform.FindChild("leyenda3").gameObject;
        Panell3 = Leyenda3.transform.FindChild("Panel").gameObject;
        tarjeta1l3 = Panell3.transform.FindChild("tarjeta1").gameObject;
        tarjeta2l3 = Panell3.transform.FindChild("tarjeta2").gameObject;
        tarjeta3l3 = Panell3.transform.FindChild("tarjeta3").gameObject;
        btnAceptarl3 = tarjeta3l3.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnsigl3 = Panell3.transform.FindChild("btnsig").GetComponent<Button>();
        btnantl3 = Panell3.transform.FindChild("btnant").GetComponent<Button>();

        Leyenda4 = Pantalla3.transform.FindChild("leyenda4").gameObject;
        Panell4 = Leyenda4.transform.FindChild("Panel").gameObject;
        tarjeta1l4 = Panell4.transform.FindChild("tarjeta1").gameObject;
        tarjeta2l4 = Panell4.transform.FindChild("tarjeta2").gameObject;
        tarjeta3l4 = Panell4.transform.FindChild("tarjeta3").gameObject;
        btnAceptarl4 = tarjeta3l4.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnsigl4 = Panell4.transform.FindChild("btnsig").GetComponent<Button>();
        btnantl4 = Panell4.transform.FindChild("btnant").GetComponent<Button>();


        Leyenda5 = Pantalla3.transform.FindChild("leyenda5").gameObject;
        Panell5 = Leyenda5.transform.FindChild("Panel").gameObject;
        tarjeta1l5 = Panell5.transform.FindChild("tarjeta1").gameObject;
        tarjeta2l5 = Panell5.transform.FindChild("tarjeta2").gameObject;
        btnAceptarl5 = tarjeta2l5.transform.FindChild("btnAceptar").GetComponent<Button>();
        btnsigl5 = Panell5.transform.FindChild("btnsig").GetComponent<Button>();
        btnantl5 = Panell5.transform.FindChild("btnant").GetComponent<Button>();


        Pantalla4 = Reto1Obj.transform.GetChild(0).FindChild("Pantalla4").gameObject;
        btnMision1 = Pantalla4.transform.FindChild("btnMision1").GetComponent<Button>();
        btnMision2 = Pantalla4.transform.FindChild("btnMision2").GetComponent<Button>();
        btnMision3 = Pantalla4.transform.FindChild("btnMision3").GetComponent<Button>();
        popupfinal = Pantalla4.transform.FindChild("popupfinal").gameObject;
        btnaceptarfinal = popupfinal.transform.FindChild("btnaceptarfinal").GetComponent<Button>();
        popuppierdevidas = Pantalla4.transform.FindChild("popuppierdevidas").gameObject;
        btnaceptarpierdevidas = popuppierdevidas.transform.FindChild("btnaceptar").GetComponent<Button>();


        //Reto 1 Mision1 Leyenda1
        Mision1OpcionLeyenda1 = Pantalla4.transform.FindChild("Mision1OpcionLeyenda1").gameObject;
        fondonegrom1l1 = Mision1OpcionLeyenda1.transform.FindChild("fondonegrom1l1").gameObject;
        popupm1l1 = Mision1OpcionLeyenda1.transform.FindChild("popupm1l1").gameObject;
        btnnom1l1 = popupm1l1.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm1l1 = popupm1l1.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom1l1 = Mision1OpcionLeyenda1.transform.FindChild("anciano").gameObject;
        globoexitom1l1 = ancianom1l1.transform.FindChild("globo").gameObject;
        globoerrorm1l1 = ancianom1l1.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision1 Leyenda2
        Mision1OpcionLeyenda2 = Pantalla4.transform.FindChild("Mision1OpcionLeyenda2").gameObject;
        fondonegrom1l2 = Mision1OpcionLeyenda2.transform.FindChild("fondonegrom1l2").gameObject;
        popupm1l2 = Mision1OpcionLeyenda2.transform.FindChild("popupm1").gameObject;
        btnnom1l2 = popupm1l2.transform.FindChild("btnno").GetComponent<Button>();
        btnsim1l2 = popupm1l2.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom1l2 = Mision1OpcionLeyenda2.transform.FindChild("anciano").gameObject;
        globoexitom1l2 = ancianom1l2.transform.FindChild("globoexito").gameObject;
        globoerrorm1l2 = ancianom1l2.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision1 Leyenda3
        Mision1OpcionLeyenda3 = Pantalla4.transform.FindChild("Mision1OpcionLeyenda3").gameObject;
        fondonegrom1l3 = Mision1OpcionLeyenda3.transform.FindChild("fondonegrom1l3").gameObject;
        popupm1l3 = Mision1OpcionLeyenda3.transform.FindChild("popupm1").gameObject;
        btnnom1l3 = popupm1l3.transform.FindChild("btnno").GetComponent<Button>();
        btnsim1l3 = popupm1l3.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom1l3 = Mision1OpcionLeyenda3.transform.FindChild("anciano").gameObject;
        globoexitom1l3 = ancianom1l3.transform.FindChild("globoexito").gameObject;
        globoerrorm1l3 = ancianom1l3.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision1 Leyenda4
        Mision1OpcionLeyenda4 = Pantalla4.transform.FindChild("Mision1OpcionLeyenda4").gameObject;
        fondonegrom1l4 = Mision1OpcionLeyenda4.transform.FindChild("fondonegrom1l4").gameObject;
        popupm1l4 = Mision1OpcionLeyenda4.transform.FindChild("popupm1").gameObject;
        btnnom1l4 = popupm1l4.transform.FindChild("btnno").GetComponent<Button>();
        btnsim1l4 = popupm1l4.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom1l4 = Mision1OpcionLeyenda4.transform.FindChild("anciano").gameObject;
        globoexitom1l4 = ancianom1l4.transform.FindChild("globoexito").gameObject;
        globoerrorm1l4 = ancianom1l4.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision1 Leyenda5
        Mision1OpcionLeyenda5 = Pantalla4.transform.FindChild("Mision1OpcionLeyenda5").gameObject;
        fondonegrom1l5 = Mision1OpcionLeyenda5.transform.FindChild("fondonegrom1l5").gameObject;
        popupm1l5 = Mision1OpcionLeyenda5.transform.FindChild("popupm1").gameObject;
        btnnom1l5 = popupm1l5.transform.FindChild("btnno").GetComponent<Button>();
        btnsim1l5 = popupm1l5.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom1l5 = Mision1OpcionLeyenda5.transform.FindChild("anciano").gameObject;
        globoexitom1l5 = ancianom1l5.transform.FindChild("globoexito").gameObject;
        globoerrorm1l5 = ancianom1l5.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision2 Leyenda1
        Mision2OpcionLeyenda1 = Pantalla4.transform.FindChild("Mision2OpcionLeyenda1").gameObject;
        fondonegrom2l1 = Mision2OpcionLeyenda1.transform.FindChild("fondonegrom2l1").gameObject;
        popupm2l1 = Mision2OpcionLeyenda1.transform.FindChild("popupm2").gameObject;
        btnnom2l1 = popupm2l1.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm2l1 = popupm2l1.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom2l1 = Mision2OpcionLeyenda1.transform.FindChild("anciano").gameObject;
        globoexitom2l1 = ancianom2l1.transform.FindChild("globoexito").gameObject;
        globoerrorm2l1 = ancianom2l1.transform.FindChild("globoerror").gameObject;

        //Reto1 Mision2 Leyenda 2
        Mision2OpcionLeyenda2 = Pantalla4.transform.FindChild("Mision2OpcionLeyenda2").gameObject;
        fondonegrom2l2 = Mision2OpcionLeyenda2.transform.FindChild("fondonegrom2l2").gameObject;
        popupm2l2 = Mision2OpcionLeyenda2.transform.FindChild("popupm2").gameObject;
        btnnom2l2 = popupm2l2.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm2l2 = popupm2l2.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom2l2 = Mision2OpcionLeyenda2.transform.FindChild("anciano").gameObject;
        globoexitom2l2 = ancianom2l2.transform.FindChild("globoexito").gameObject;
        globoerrorm2l2 = ancianom2l2.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision2 Leyenda 3
        Mision2OpcionLeyenda3 = Pantalla4.transform.FindChild("Mision2OpcionLeyenda3").gameObject;
        fondonegrom2l3 = Mision2OpcionLeyenda3.transform.FindChild("fondonegrom2l3").gameObject;
        popupm2l3 = Mision2OpcionLeyenda3.transform.FindChild("popupm2").gameObject;
        btnnom2l3 = popupm2l3.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm2l3 = popupm2l3.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom2l3 = Mision2OpcionLeyenda3.transform.FindChild("anciano").gameObject;
        globoexitom2l3 = ancianom2l3.transform.FindChild("globoexito").gameObject;
        globoerrorm2l3 = ancianom2l3.transform.FindChild("globoerror").gameObject;

        //Reto 1 Mision 2 Leyenda 4
        Mision2OpcionLeyenda4 = Pantalla4.transform.FindChild("Mision2OpcionLeyenda4").gameObject;
        fondonegrom2l4 = Mision2OpcionLeyenda4.transform.FindChild("fondonegrom2l4").gameObject;
        popupm2l4 = Mision2OpcionLeyenda4.transform.FindChild("popupm2").gameObject;
        btnnom2l4 = popupm2l4.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm2l4 = popupm2l4.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom2l4 = Mision2OpcionLeyenda4.transform.FindChild("anciano").gameObject;
        globoexitom2l4 = ancianom2l4.transform.FindChild("globoexito").gameObject;
        globoerrorm2l4 = ancianom2l4.transform.FindChild("globoerror").gameObject;

        //Reto1 Mision 2 Leyenda 5
        Mision2OpcionLeyenda5 = Pantalla4.transform.FindChild("Mision2OpcionLeyenda5").gameObject;
        fondonegrom2l5 = Mision2OpcionLeyenda5.transform.FindChild("fondonegrom2l5").gameObject;
        popupm2l5 = Mision2OpcionLeyenda5.transform.FindChild("popupm2").gameObject;
        btnnom2l5 = popupm2l5.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm2l5 = popupm2l5.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom2l5 = Mision2OpcionLeyenda5.transform.FindChild("anciano").gameObject;
        globoexitom2l5 = ancianom2l5.transform.FindChild("globoexito").gameObject;
        globoerrorm2l5 = ancianom2l5.transform.FindChild("globoerror").gameObject;


        //Reto1 Mision3 Leyenda1
        Mision3OpcionLeyenda1 = Pantalla4.transform.FindChild("Mision3OpcionLeyenda1").gameObject;
        fondonegrom3l1 = Mision3OpcionLeyenda1.transform.FindChild("fondonegrom3l1").gameObject;
        popupm3l1 = Mision3OpcionLeyenda1.transform.FindChild("popupm3").gameObject;
        btnnom3l1 = popupm3l1.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm3l1 = popupm3l1.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom3l1 = Mision3OpcionLeyenda1.transform.FindChild("anciano").gameObject;
        globoexitom3l1 = ancianom3l1.transform.FindChild("globoexito").gameObject;
        globoerrorm3l1 = ancianom3l1.transform.FindChild("globoerror").gameObject;


        //Reto1 Mision 3 Leyenda 2
        Mision3OpcionLeyenda2 = Pantalla4.transform.FindChild("Mision3OpcionLeyenda2").gameObject;
        fondonegrom3l2 = Mision3OpcionLeyenda2.transform.FindChild("fondonegrom3l2").gameObject;
        popupm3l2 = Mision3OpcionLeyenda2.transform.FindChild("popupm3").gameObject;
        btnnom3l2 = popupm3l2.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm3l2 = popupm3l2.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom3l2 = Mision3OpcionLeyenda2.transform.FindChild("anciano").gameObject;
        globoexitom3l2 = ancianom3l2.transform.FindChild("globoexito").gameObject;
        globoerrorm3l2 = ancianom3l2.transform.FindChild("globoerror").gameObject;

        //Reto1 Mision3 Leyenda 3
        Mision3OpcionLeyenda3 = Pantalla4.transform.FindChild("Mision3OpcionLeyenda3").gameObject;
        fondonegrom3l3 = Mision3OpcionLeyenda3.transform.FindChild("fondonegrom3l3").gameObject;
        popupm3l3 = Mision3OpcionLeyenda3.transform.FindChild("popupm3").gameObject;
        btnnom3l3 = popupm3l3.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm3l3 = popupm3l3.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom3l3 = Mision3OpcionLeyenda3.transform.FindChild("anciano").gameObject;
        globoexitom3l3 = ancianom3l3.transform.FindChild("globoexito").gameObject;
        globoerrorm3l3 = ancianom3l3.transform.FindChild("globoerror").gameObject;

        //Reto1 Mision3 Leyenda 4
        Mision3OpcionLeyenda4 = Pantalla4.transform.FindChild("Mision3OpcionLeyenda4").gameObject;
        fondonegrom3l4 = Mision3OpcionLeyenda4.transform.FindChild("fondonegrom3l4").gameObject;
        popupm3l4 = Mision3OpcionLeyenda4.transform.FindChild("popupm3").gameObject;
        btnnom3l4 = popupm3l4.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm3l4 = popupm3l4.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom3l4 = Mision3OpcionLeyenda4.transform.FindChild("anciano").gameObject;
        globoexitom3l4 = ancianom3l4.transform.FindChild("globoexito").gameObject;
        globoerrorm3l4 = ancianom3l4.transform.FindChild("globoerror").gameObject;


        //Reto1 Mision3 Leyenda 5
        Mision3OpcionLeyenda5 = Pantalla4.transform.FindChild("Mision3OpcionLeyenda5").gameObject;
        fondonegrom3l5 = Mision3OpcionLeyenda5.transform.FindChild("fondonegrom3l5").gameObject;
        popupm3l5 = Mision3OpcionLeyenda5.transform.FindChild("popupm3").gameObject;
        btnnom3l5 = popupm3l5.transform.FindChild("btnno").GetComponent<Button>();
        btnaceptarm3l5 = popupm3l5.transform.FindChild("btnaceptar").GetComponent<Button>();
        ancianom3l5 = Mision3OpcionLeyenda5.transform.FindChild("anciano").gameObject;
        globoexitom3l5 = ancianom3l5.transform.FindChild("globoexito").gameObject;
        globoerrorm3l5 = ancianom3l5.transform.FindChild("globoerror").gameObject;

        Reto2Obj = transform.FindChild("Reto2").gameObject;
        Reto2Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;

        Reto3Obj = transform.FindChild("Reto3").gameObject;
        Reto3Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;

        Reto4Obj = transform.FindChild("Reto4").gameObject;
        Reto4Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;

        RetoFinalObj = transform.FindChild("RetoFinal").gameObject;
        RetoFinalObj.transform.GetComponent<Canvas>().worldCamera = Camera.main;

        //menu.SetActive(false);

        btnentrar.onClick.AddListener(delegate
        {
            
            menu.SetActive(true);
            soundManager.playSound("Sound_SonidoFondo2", 0);
			//PlayerPrefs.SetInt("corazonesODA2_ODA2", 0);
			cerrarIndex();
        });


        btnaceptarpierdevidas.onClick.AddListener(delegate
        {
            popuppierdevidas.SetActive(false);
            Pantalla4.SetActive(false);
            Pantalla3.SetActive(false);
            rand2 = Random.Range(1, 6); //random para la aparición aleatoria de las leyendas
            rand = rand2;
				/*Destroy(this.gameObject);
				this.GetComponent<Reto1Oda2> ().Clon.name = "ODA2";
				this.GetComponent<Reto1Oda2>().Clon.SetActive(true);*/
            //seleccionarReto(btnReto1);

				menu.SetActive(true);
				Reto1Obj.SetActive(true);
				Pantalla2.SetActive(true);
				soundManager.playSound("Fondo_Cuervo", 0);
				fondonegro.SetActive(true);
				popupp2.SetActive(true);
				soundManager.playSound("Sound_OpenPopUp10", 1);

            m1 = false;
            m2 = false;
            m3 = false;
				m1a = false;
				m2a = false;
				m3a = false;
            btnMision1.gameObject.SetActive(true);
            btnMision1.interactable = true;
            btnMision2.gameObject.SetActive(true);
            btnMision2.interactable = true;
            btnMision3.gameObject.SetActive(true);
            btnMision3.interactable = true;
            //desactivar todas las misiones
            Mision1OpcionLeyenda1.SetActive(false);
            popupm1l1.SetActive(false);
            ancianom1l1.SetActive(false);
            globoexitom1l1.SetActive(false);
            globoerrorm1l1.SetActive(false);

            Mision1OpcionLeyenda2.SetActive(false);
            popupm1l2.SetActive(false);
            ancianom1l2.SetActive(false);
            globoexitom1l2.SetActive(false);
            globoerrorm1l2.SetActive(false);

            Mision1OpcionLeyenda3.SetActive(false);
            popupm1l3.SetActive(false);
            ancianom1l3.SetActive(false);
            globoexitom1l3.SetActive(false);
            globoerrorm1l3.SetActive(false);

            Mision1OpcionLeyenda4.SetActive(false);
            popupm1l4.SetActive(false);
            ancianom1l4.SetActive(false);
            globoexitom1l4.SetActive(false);
            globoerrorm1l4.SetActive(false);

            Mision1OpcionLeyenda5.SetActive(false);
            popupm1l5.SetActive(false);
            ancianom1l5.SetActive(false);
            globoexitom1l5.SetActive(false);
            globoerrorm1l5.SetActive(false);

            Mision2OpcionLeyenda1.SetActive(false);
            popupm2l1.SetActive(false);
            ancianom2l1.SetActive(false);
            globoexitom2l1.SetActive(false);
            globoerrorm2l1.SetActive(false);


            Mision2OpcionLeyenda2.SetActive(false);
            popupm2l2.SetActive(false);
            ancianom2l2.SetActive(false);
            globoexitom2l2.SetActive(false);
            globoerrorm2l2.SetActive(false);

            Mision2OpcionLeyenda3.SetActive(false);
            popupm2l3.SetActive(false);
            ancianom2l3.SetActive(false);
            globoexitom2l3.SetActive(false);
            globoerrorm2l3.SetActive(false);

            Mision2OpcionLeyenda4.SetActive(false);
            popupm2l4.SetActive(false);
            ancianom2l4.SetActive(false);
            globoexitom2l4.SetActive(false);
            globoerrorm2l4.SetActive(false);

            Mision2OpcionLeyenda5.SetActive(false);
            popupm2l5.SetActive(false);
            ancianom2l5.SetActive(false);
            globoexitom2l5.SetActive(false);
            globoerrorm2l5.SetActive(false);

            Mision3OpcionLeyenda1.SetActive(false);
            popupm3l1.SetActive(false);
            ancianom3l1.SetActive(false);
            globoexitom3l1.SetActive(false);
            globoerrorm3l1.SetActive(false);

            Mision3OpcionLeyenda2.SetActive(false);
            popupm3l2.SetActive(false);
            ancianom3l2.SetActive(false);
            globoexitom3l2.SetActive(false);
            globoerrorm3l2.SetActive(false);

            Mision3OpcionLeyenda3.SetActive(false);
            popupm3l3.SetActive(false);
            ancianom3l3.SetActive(false);
            globoexitom3l3.SetActive(false);
            globoerrorm3l3.SetActive(false);

            Mision3OpcionLeyenda4.SetActive(false);
            popupm3l4.SetActive(false);
            ancianom3l4.SetActive(false);
            globoexitom3l4.SetActive(false);
            globoerrorm3l4.SetActive(false);

            Mision3OpcionLeyenda5.SetActive(false);
            popupm3l5.SetActive(false);
            ancianom3l5.SetActive(false);
            globoexitom3l5.SetActive(false);
            globoerrorm3l5.SetActive(false);
        });

        btnReto1.onClick.AddListener(delegate
        {
            rand = Random.Range(1, 6); //random para la aparición aleatoria de las leyendas
            seleccionarReto(btnReto1);
        });
        btnReto2.onClick.AddListener(delegate { seleccionarReto(btnReto2); });
        btnReto3.onClick.AddListener(delegate { seleccionarReto(btnReto3); });
        btnReto4.onClick.AddListener(delegate { seleccionarReto(btnReto4); });
        btnRetoFinal.onClick.AddListener(delegate { seleccionarReto(btnRetoFinal); });


        btnaceptar.onClick.AddListener(delegate
        {
            popupp2.SetActive(false);
            fondonegro.SetActive(false);
            Pantalla3.SetActive(true);
            //reinicio = true;
        });


        btnaceptarp3.onClick.AddListener(delegate
        {
            if (mostrarley)
            {
                popupp3.SetActive(false);
                fondonegro2.SetActive(false);
                //activar random de leyendas 
                bandera = 1;
                bandera2 = 1;
                Pantalla3.SetActive(true);
                Invoke("mostrarleyenda", 0f);
            }
            else
            {
                bandera = 1;
                bandera2 = 1;
                Pantalla3.SetActive(true);
                Invoke("mostrarleyenda", 0f);
                mostrarley = true;
            }
        });


        btnfogata.onClick.AddListener(delegate
        {
            if (mostrarley == false)
            {
                fondonegro2.SetActive(true);
                popupp3.SetActive(true);
                soundManager.playSound("Sound_OpenPopUp10", 1);
                Pantalla4.SetActive(false);
                Pantalla3.SetActive(true);
                mostrarley = true;
            }
            else
            {
                Invoke("mostrarleyenda", 0f);
                Pantalla3.SetActive(true);
                Pantalla4.SetActive(false);
            }
            
            if (reinicio && (rand == 1))
            {
                Leyenda1.SetActive(true);
                tarjeta1l1.SetActive(true);
                btnsig.gameObject.SetActive(true);
                banderaleyenda = 1;
            }
            if (reinicio && (rand == 2))
            {
                Leyenda2.SetActive(true);
                tarjeta1l2.SetActive(true);
                btnsigl2.gameObject.SetActive(true);
                banderaleyenda = 2;
            }

            if (reinicio && (rand == 3))
            {
                Leyenda3.SetActive(true);
                tarjeta1l3.SetActive(true);
                btnsigl3.gameObject.SetActive(true);
                banderaleyenda = 3;
            }

            if (reinicio && (rand == 4))
            {
                Leyenda4.SetActive(true);
                tarjeta1l4.SetActive(true);
                btnsigl4.gameObject.SetActive(true);
                banderaleyenda = 4;
            }

            if (reinicio && (rand == 5))
            {
                Leyenda5.SetActive(true);
                tarjeta1l5.SetActive(true);
                btnsigl5.gameObject.SetActive(true);
                banderaleyenda = 5;
            }


            //Invoke("mostrarleyenda", 0f);
        });


        btnsig.onClick.AddListener(delegate
        {
            if (bandera == 1)
            {
                //tarjeta1l1.SetActive(false);
                btnsig.gameObject.SetActive(true);
                btnant.gameObject.SetActive(true);
                bandera = 2;
                bandera2 = 2;
                tarjeta2l1.SetActive(true);
                tarjeta1l1.SetActive(false);
            }
            else
            {
                if (bandera == 2)
                {
                    bandera = 3;
                    bandera2 = 3;

                    tarjeta3l1.SetActive(true);
                    tarjeta2l1.SetActive(false);
                    btnsig.gameObject.SetActive(true);
                    btnant.gameObject.SetActive(true);
                }
                else
                {
                    if (bandera == 3)
                    {
                        //tarjeta1l1.SetActive(false);
                        bandera = 4;
                        bandera2 = 4;
                        tarjeta4l1.SetActive(true);
                        tarjeta3l1.SetActive(false);
                        btnsig.gameObject.SetActive(false);
                        btnant.gameObject.SetActive(true);
                        btnsig.gameObject.SetActive(false);
                    }
                }
            }
        });


        btnant.onClick.AddListener(delegate
        {
            if (bandera2 == 2)
            {
                tarjeta1l1.SetActive(true);
                tarjeta2l1.SetActive(false);
                bandera = 1;
                bandera2 = 1;

                btnsig.gameObject.SetActive(true);
                btnant.gameObject.SetActive(false);
            }
            else
            {
                if (bandera2 == 3)
                {
                    tarjeta2l1.SetActive(true);
                    tarjeta3l1.SetActive(false);
                    bandera = 2;
                    bandera2 = 2;
                    btnant.gameObject.SetActive(true);
                    btnsig.gameObject.SetActive(true);
                }
                else
                {
                    if (bandera2 == 4)
                    {
                        //tarjeta1l1.SetActive(false);

                        tarjeta4l1.SetActive(false);
                        tarjeta3l1.SetActive(true);
                        bandera = 3;
                        bandera2 = 3;
                        btnant.gameObject.SetActive(true);
                        btnsig.gameObject.SetActive(true);
                    }
                }
            }
        });

        //botón aceptar de leyenda1
        btnAceptar.onClick.AddListener(delegate
        {
            Leyenda1.SetActive(false);
            bandera = 0;
            bandera2 = 0;
            Pantalla2.SetActive(false);
            Pantalla3.SetActive(true);
            Pantalla4.SetActive(true);

            //prender mision 1 de leyenda1
            //Mision1OpcionLeyenda1.SetActive(true);
            //volver leyenda a su estado original
            tarjeta4l1.SetActive(false);
            tarjeta1l1.SetActive(true);
            btnsig.gameObject.SetActive(true);
            bandera = 1;
            bandera2 = 0;
        });


        //botones para leyenda 2

        btnsigl2.onClick.AddListener(delegate
        {
            if (bandera == 1)
            {
                //tarjeta1l1.SetActive(false);
                bandera = 2;
                bandera2 = 2;
                tarjeta2l2.SetActive(true);
                tarjeta1l2.SetActive(false);
                btnantl2.gameObject.SetActive(true);
                btnsigl2.gameObject.SetActive(false);
            }
        });


        btnantl2.onClick.AddListener(delegate
        {
            if (bandera2 == 2)
            {
                tarjeta1l2.SetActive(true);
                tarjeta2l2.SetActive(false);
                bandera = 1;
                bandera2 = 1;

                btnsigl2.gameObject.SetActive(true);
                btnantl2.gameObject.SetActive(false);
            }
        });


        btnAceptarl2.onClick.AddListener(delegate
        {
            Leyenda2.SetActive(false);
            bandera = 0;
            bandera2 = 0;
            Pantalla3.SetActive(true);
            Pantalla4.SetActive(true);
            //volver la leyenda a su estado original
            //volver leyenda a su estado original
            tarjeta2l2.SetActive(false);
            tarjeta1l2.SetActive(true);
            btnsigl2.gameObject.SetActive(true);
            bandera = 1;
            bandera2 = 0;
        });


        //botones para leyenda3, 3 tarjetas 
        btnsigl3.onClick.AddListener(delegate
        {
            if (bandera == 1)
            {
                //tarjeta1l1.SetActive(false);
                bandera = 2;
                bandera2 = 2;
                tarjeta2l3.SetActive(true);
                tarjeta1l3.SetActive(false);
                btnantl3.gameObject.SetActive(true);
                btnsigl3.gameObject.SetActive(true);
            }
            else
            {
                if (bandera == 2)
                {
                    bandera = 3;
                    bandera2 = 3;
                    tarjeta3l3.SetActive(true);
                    tarjeta2l3.SetActive(false);
                    btnantl3.gameObject.SetActive(true);
                    btnsigl3.gameObject.SetActive(false);
                }
            }
        });


        btnantl3.onClick.AddListener(delegate
        {
            if (bandera2 == 2)
            {
                tarjeta1l3.SetActive(true);
                tarjeta2l3.SetActive(false);
                bandera = 1;
                bandera2 = 1;

                btnsigl3.gameObject.SetActive(true);
                btnantl3.gameObject.SetActive(false);
            }
            else
            {
                if (bandera2 == 3)
                {
                    tarjeta2l3.SetActive(true);
                    tarjeta3l3.SetActive(false);
                    bandera = 2;
                    bandera2 = 2;
                    btnantl3.gameObject.SetActive(true);
                    btnsigl3.gameObject.SetActive(true);
                }
            }
        });


        btnAceptarl3.onClick.AddListener(delegate
        {
            Leyenda3.SetActive(false);
            bandera = 0;
            bandera2 = 0;
            Pantalla2.SetActive(false);
            Pantalla3.SetActive(true);
            Pantalla4.SetActive(true);
            //regresar leyenda a su estado original
            tarjeta3l3.SetActive(false);
            tarjeta1l3.SetActive(true);
            btnsigl3.gameObject.SetActive(true);
            bandera = 1;
            bandera2 = 0;
        });


        //botones para leyenda 4, 3 tarjetas

        btnsigl4.onClick.AddListener(delegate
        {
            if (bandera == 1)
            {
                //tarjeta1l1.SetActive(false);
                bandera = 2;
                bandera2 = 2;
                tarjeta2l4.SetActive(true);
                tarjeta1l4.SetActive(false);
                btnantl4.gameObject.SetActive(true);
                btnsigl4.gameObject.SetActive(true);
            }
            else
            {
                if (bandera == 2)
                {
                    bandera = 3;
                    bandera2 = 3;
                    tarjeta3l4.SetActive(true);
                    tarjeta2l4.SetActive(false);
                    btnantl4.gameObject.SetActive(true);
                    btnsigl4.gameObject.SetActive(false);
                }
            }
        });


        btnantl4.onClick.AddListener(delegate
        {
            if (bandera2 == 2)
            {
                tarjeta1l4.SetActive(true);
                tarjeta2l4.SetActive(false);
                bandera = 1;
                bandera2 = 1;

                btnsigl4.gameObject.SetActive(true);
                btnantl4.gameObject.SetActive(false);
            }
            else
            {
                if (bandera2 == 3)
                {
                    tarjeta2l4.SetActive(true);
                    tarjeta3l4.SetActive(false);
                    bandera = 2;
                    bandera2 = 2;
                    btnantl4.gameObject.SetActive(true);
                    btnsigl4.gameObject.SetActive(true);
                }
            }
        });


        btnAceptarl4.onClick.AddListener(delegate
        {
            Leyenda4.SetActive(false);
            bandera = 0;
            bandera2 = 0;
            Pantalla2.SetActive(false);
            Pantalla3.SetActive(true);
            Pantalla4.SetActive(true);
            //regresar leyenda a su estado original
            tarjeta3l4.SetActive(false);
            tarjeta1l4.SetActive(true);
            btnsigl4.gameObject.SetActive(true);
            bandera = 1;
            bandera2 = 0;
        });


        //botones para leyenda 5, 2 tarjetas
        btnsigl5.onClick.AddListener(delegate
        {
            if (bandera == 1)
            {
                //tarjeta1l1.SetActive(false);
                bandera = 2;
                bandera2 = 2;
                tarjeta2l5.SetActive(true);
                tarjeta1l5.SetActive(false);
                btnantl5.gameObject.SetActive(true);
                btnsigl5.gameObject.SetActive(false);
            }
        });


        btnantl5.onClick.AddListener(delegate
        {
            if (bandera2 == 2)
            {
                tarjeta1l5.SetActive(true);
                tarjeta2l5.SetActive(false);
                bandera = 1;
                bandera2 = 1;

                btnsigl5.gameObject.SetActive(true);
                btnantl5.gameObject.SetActive(false);
            }
        });


        btnAceptarl5.onClick.AddListener(delegate
        {
            Leyenda5.SetActive(false);
            bandera = 0;
            bandera2 = 0;
            Pantalla2.SetActive(false);
            Pantalla3.SetActive(true);
            Pantalla4.SetActive(true);
            //regresar leyenda a su estado original
            tarjeta2l5.SetActive(false);
            tarjeta1l5.SetActive(true);
            btnsigl5.gameObject.SetActive(true);
            bandera = 1;
            bandera2 = 0;
        });

       
        //botón de Mision1 Pantalla 4, Leyenda 1-5
        btnMision1.onClick.AddListener(delegate
        {
			btnMision2.interactable=true;
			btnMision3.interactable=true;
			corazonesreto= PlayerPrefs.GetInt("corazonesODA2_ODA2");
			Mision1OpcionLeyenda1.SetActive(false);
            Mision1OpcionLeyenda2.SetActive(false);
            Mision1OpcionLeyenda3.SetActive(false);
            Mision1OpcionLeyenda4.SetActive(false);
            Mision1OpcionLeyenda5.SetActive(false);
            m1 = true;
            banderamision = true;

            if (banderaleyenda == 1)
            {
                Mision1OpcionLeyenda1.SetActive(true);
                fondonegrom1l1.SetActive(true);
                popupm1l1.SetActive(true);
                soundManager.playSound("Sound_OpenPopUp10", 1);
            }
            else
            {
                if (banderaleyenda == 2)
                {
                    Mision1OpcionLeyenda2.SetActive(true);
                    fondonegrom1l2.SetActive(true);
                    popupm1l2.SetActive(true);
                    soundManager.playSound("Sound_OpenPopUp10", 1);
                }
                else
                {
                    if (banderaleyenda == 3)
                    {
                        Mision1OpcionLeyenda3.SetActive(true);
                        fondonegrom1l3.SetActive(true);
                        popupm1l3.SetActive(true);
                        soundManager.playSound("Sound_OpenPopUp10", 1);
                    }

                    else
                    {
                        if (banderaleyenda == 4)
                        {
                            Mision1OpcionLeyenda4.SetActive(true);
                            fondonegrom1l4.SetActive(true);
                            popupm1l4.SetActive(true);
                            soundManager.playSound("Sound_OpenPopUp10", 1);
                        }
                        else
                        {
                            if (banderaleyenda == 5)
                            {
                                Mision1OpcionLeyenda5.SetActive(true);
                                fondonegrom1l5.SetActive(true);
                                popupm1l5.SetActive(true);
                                soundManager.playSound("Sound_OpenPopUp10", 1);
                            }
                        }
                    }
                }
            }
            //Invoke("mostrarpopfinal", 0f);



        });

        //boton aceptar de popup de mision1leyenda1

        btnaceptarm1l1.onClick.AddListener(delegate
        {
            fondonegrom1l1.SetActive(false);
            popupm1l1.SetActive(false);
            btnMision1.interactable = false;
			
            ancianom1l1.SetActive(true);
            globoexitom1l1.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom1l1.transform.GetChild(0).GetComponent<Text>();
            animateString(14f,
                "¡Hiciste muy bien! Tu vida no corría peligro pues lo que se contó en la leyenda sobre el bosque es ficticio.");
			btnMision2.interactable = false;
			btnMision3.interactable = false;
            numcorazones++;
			m1a=true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
			Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision1leyenda1
        btnnom1l1.onClick.AddListener(delegate
        {
            fondonegrom1l1.SetActive(false);
            popupm1l1.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l1.SetActive(true);
            globoexitom1l1.SetActive(false);
            globoerrorm1l1.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm1l1.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "Recuerda que el reto es distinguir la realidad y la fantasía de la leyenda. Tu vida no corría peligro pues lo que se contó en la leyenda sobre el bosque es ficticio.");

			btnMision2.interactable = false;
			btnMision3.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton aceptar de popup de mision1leyenda2

        btnsim1l2.onClick.AddListener(delegate
        {
            fondonegrom1l2.SetActive(false);
            popupm1l2.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l2.SetActive(true);
            globoerrorm1l2.SetActive(true);
            label = globoerrorm1l2.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "Recuerda que el reto es distinguir la realidad y la fantasía de la leyenda. No es posible saber cuál figura es la original porque es un hecho fantástico.");

			btnMision2.interactable = false;
			btnMision3.interactable = false;
            numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision1leyenda2
        btnnom1l2.onClick.AddListener(delegate
        {
            fondonegrom1l2.SetActive(false);
            popupm1l2.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l2.SetActive(true);
            globoerrorm1l2.SetActive(false);
            globoexitom1l2.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom1l2.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                " ¡Una decisión correcta! No es posible saber cuál figura es la original porque está basada en un hecho fantástico.");


            numcorazones++;
			m1a=true;
			btnMision2.interactable = false;
			btnMision3.interactable = false;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
			Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision1leyenda3

        btnsim1l3.onClick.AddListener(delegate
        {
            fondonegrom1l3.SetActive(false);
            popupm1l3.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l3.SetActive(true);
            globoerrorm1l3.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm1l3.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "No todos los elementos de una leyenda son ficticios, es casi imposible ver al Sol directamente e intentarlo puede dañar nuestros ojos.");

            numcorazones--;

			btnMision2.interactable = false;
			btnMision3.interactable = false;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision1leyenda3
        btnnom1l3.onClick.AddListener(delegate
        {
            fondonegrom1l3.SetActive(false);
            popupm1l3.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l3.SetActive(true);
            globoerrorm1l3.SetActive(false);
            globoexitom1l3.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom1l3.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "¡Excelente decisión! Es casi imposible ver al Sol directamente e intentarlo puede dañar nuestros ojos. Hiciste bien al negarte.");

			btnMision2.interactable = false;
			btnMision3.interactable = false;
			numcorazones++;
				m1a=true;
			//corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision1leyenda4

        btnsim1l4.onClick.AddListener(delegate
        {
            fondonegrom1l4.SetActive(false);
            popupm1l4.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l4.SetActive(true);
            globoerrorm1l4.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm1l4.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "Recuerda que el reto es distinguir la realidad y la fantasía. Es imposible saber cuál era la melodía debido a que no era real.");

			btnMision2.interactable = false;
			btnMision3.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision1leyenda4
        btnnom1l4.onClick.AddListener(delegate
        {
            fondonegrom1l4.SetActive(false);
            popupm1l4.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l4.SetActive(true);
            globoerrorm1l4.SetActive(false);
            globoexitom1l4.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom1l4.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "¡Una decisión correcta! La melodía del flautista es un elemento de fantasía por lo que esta misión es imposible.");

            
			btnMision2.interactable = false;
			btnMision3.interactable = false;
			numcorazones++;
				m1a=true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision1leyenda5

        btnsim1l5.onClick.AddListener(delegate
        {
            fondonegrom1l5.SetActive(false);
            popupm1l5.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l5.SetActive(true);
            globoerrorm1l5.SetActive(false);
            globoexitom1l5.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom1l5.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                " En la leyenda, el café que desorienta es un elemento fantástico, por lo tanto esta misión era realizable.");

            
			btnMision2.interactable = false;
			btnMision3.interactable = false;
			numcorazones++;
				m1a=true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision1leyenda5
        btnnom1l5.onClick.AddListener(delegate
        {
            fondonegrom1l5.SetActive(false);
            popupm1l5.SetActive(false);
            btnMision1.interactable = false;
            ancianom1l5.SetActive(true);
            globoerrorm1l5.SetActive(true);
            globoexitom1l5.SetActive(false);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm1l5.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "Aunque no es recomendable aceptar bebidas por parte de desconocidos, en la leyenda, el café que desorienta es un elemento fantástico. ");

			btnMision2.interactable = false;
			btnMision3.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //botones para Mision2 Pantalla 4, Leyenda 1
        //botón de Mision2 Pantalla 4, Leyenda 1
        btnMision2.onClick.AddListener(delegate
        {
            //Mision2OpcionLeyenda1.SetActive(true);
            //fondonegrom2l1.SetActive(true);
            //popupm2l1.SetActive(true);
            //banderamision=true;
            Mision1OpcionLeyenda1.SetActive(false);
            Mision1OpcionLeyenda2.SetActive(false);
            Mision1OpcionLeyenda3.SetActive(false);
            Mision1OpcionLeyenda4.SetActive(false);
            Mision1OpcionLeyenda5.SetActive(false);

            Mision2OpcionLeyenda1.SetActive(false);
            Mision2OpcionLeyenda2.SetActive(false);
            Mision2OpcionLeyenda3.SetActive(false);
            Mision2OpcionLeyenda4.SetActive(false);
            Mision2OpcionLeyenda5.SetActive(false);
			btnMision1.interactable=true;
			btnMision3.interactable=true;
            banderamision = true;
            m2 = true;

            if (banderaleyenda == 1)
            {
                Mision2OpcionLeyenda1.SetActive(true);
                fondonegrom2l1.SetActive(true);
                popupm2l1.SetActive(true);
                soundManager.playSound("Sound_OpenPopUp10", 1);
            }
            else
            {
                if (banderaleyenda == 2)
                {
                    Mision2OpcionLeyenda2.SetActive(true);
                    fondonegrom2l2.SetActive(true);
                    popupm2l2.SetActive(true);
                    soundManager.playSound("Sound_OpenPopUp10", 1);
                }
                else
                {
                    if (banderaleyenda == 3)
                    {
                        Mision2OpcionLeyenda3.SetActive(true);
                        fondonegrom2l3.SetActive(true);
                        popupm2l3.SetActive(true);
                        soundManager.playSound("Sound_OpenPopUp10", 1);
                    }

                    else
                    {
                        if (banderaleyenda == 4)
                        {
                            Mision2OpcionLeyenda4.SetActive(true);
                            fondonegrom2l4.SetActive(true);
                            popupm2l4.SetActive(true);
                            soundManager.playSound("Sound_OpenPopUp10", 1);
                        }
                        else
                        {
                            if (banderaleyenda == 5)
                            {
                                Mision2OpcionLeyenda5.SetActive(true);
                                fondonegrom2l5.SetActive(true);
                                popupm2l5.SetActive(true);
                                soundManager.playSound("Sound_OpenPopUp10", 1);
                            }
                        }
                    }
                }
            }
            //Invoke("mostrarpopfinal", 0f);
				//si no ha hecho la mision3
				if (m1==true && m2 == true && m3==false) {
					btnMision3.gameObject.SetActive(true);
					btnMision3.interactable = true;
					//btnMision2.gameObject.SetActive(false);
					btnMision2.interactable = false;
					//btnMision1.gameObject.SetActive(false);
					btnMision1.interactable = false;

				}


        });

        //boton aceptar de popup de mision2 leyenda1

        btnaceptarm2l1.onClick.AddListener(delegate
        {
            fondonegrom2l1.SetActive(false);
            popupm2l1.SetActive(false);

            btnMision2.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l1.SetActive(true);
            globoerrorm2l1.SetActive(true);
            globoexitom2l1.SetActive(false);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm2l1.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "No todos los elementos de una leyenda son ficticios, cualquier río con la corriente alta es peligroso.");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
            numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision2 leyenda1
        btnnom2l1.onClick.AddListener(delegate
        {
            fondonegrom2l1.SetActive(false);
            popupm2l1.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l1.SetActive(true);
            globoexitom2l1.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            globoerrorm2l1.SetActive(false);
            label = globoexitom2l1.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "El peligro que existe al cruzar una corriente alta es real, hiciste muy bien al no arriesgarte.");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
            numcorazones++;
				m2a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision2 leyenda2

        btnaceptarm2l2.onClick.AddListener(delegate
        {
            fondonegrom2l2.SetActive(false);
            popupm2l2.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            ancianom2l2.SetActive(true);
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            globoexitom2l2.SetActive(false);
            globoerrorm2l2.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm2l2.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "El árbol no es real, has expuesto la salud del sastre al llevarlo al bosque en busca de lluvia y viento. ");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
            numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision2leyenda2
        btnnom2l2.onClick.AddListener(delegate
        {
            fondonegrom2l2.SetActive(false);
            popupm2l2.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l2.SetActive(true);
            globoexitom2l2.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            globoerrorm2l2.SetActive(false);
            label = globoexitom2l2.transform.GetChild(0).GetComponent<Text>();
				animateString(14f, "¡Una decisión correcta! El árbol de la leyenda no es real.");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
            numcorazones++;
				m2a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision2 leyenda3

        btnaceptarm2l3.onClick.AddListener(delegate
        {
            fondonegrom2l3.SetActive(false);
            popupm2l3.SetActive(false);
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            ancianom2l3.SetActive(true);
            globoexitom2l3.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom2l3.transform.GetChild(0).GetComponent<Text>();
				animateString(14f, "¡Muy bien! Los árboles necesitan del Sol para poder vivir.");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
			numcorazones++;
				m2a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision2leyenda3
        btnnom2l3.onClick.AddListener(delegate
        {
            fondonegrom2l3.SetActive(false);
            popupm2l3.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l3.SetActive(true);
            globoexitom2l3.SetActive(false);
            globoerrorm2l3.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm2l3.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "Los árboles necesitan de la luz solar para poder vivir. Dejaste pasar la oportunidad de salvarlos.");

            
			btnMision1.interactable = false;
			btnMision3.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision2 leyenda4

        btnaceptarm2l4.onClick.AddListener(delegate
        {
            fondonegrom2l4.SetActive(false);
            popupm2l4.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l4.SetActive(true);
            globoexitom2l4.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom2l4.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "¡Excelente decisión! El peligro de las fogatas que cuenta la leyenda no es real y ahora tienen una fuente de calor para pasar la noche.");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
			numcorazones++;
				m2a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision2leyenda4
        btnnom2l4.onClick.AddListener(delegate
        {
            fondonegrom2l4.SetActive(false);
            popupm2l4.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l4.SetActive(true);
            globoexitom2l4.SetActive(false);
            soundManager.playSound("Sound_incorrecto19", 1);
            globoerrorm2l4.SetActive(true);
            label = globoerrorm2l4.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "Los coyotes no se tornan en llamas, es un elemento fantástico, no es real.  ¡Tú y tu amigo pasarán una noche muy fría!");

			btnMision1.interactable = false;
			btnMision3.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion",10f);
            Invoke("mostrarpopfinal", 17f);
        });


        //boton aceptar de popup de mision2 leyenda5

        btnaceptarm2l5.onClick.AddListener(delegate
        {
            fondonegrom2l5.SetActive(false);
            popupm2l5.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l5.SetActive(true);
            globoexitom2l5.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom2l5.transform.GetChild(0).GetComponent<Text>();
			animateString(14f, "¡Correcto! Un caballo puede perderse en el bosque, no es un elemento fantástico.");

            
			btnMision1.interactable = false;
			btnMision3.interactable = false;
			numcorazones++;
				m2a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });

        //boton no aceptar de popup mision2leyenda5
        btnnom2l5.onClick.AddListener(delegate
        {
            fondonegrom2l5.SetActive(false);
            popupm2l5.SetActive(false);
            btnMision2.interactable = false;
            btnMision1.interactable = true;
            btnMision3.interactable = true;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom2l5.SetActive(true);
            globoexitom2l5.SetActive(false);
            globoerrorm2l5.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm2l5.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "Recuerda que el reto es distinguir la realidad y la fantasía. Un caballo puede perderse en el bosque, realizar esta misión era posible.");

            
			btnMision1.interactable = false;
			btnMision3.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);
            Invoke("mostrarpopfinal", 17f);
        });


        //botón de Mision3 Pantalla 4, Leyenda 1
        btnMision3.onClick.AddListener(delegate
        {
            Mision1OpcionLeyenda1.SetActive(false);
            Mision1OpcionLeyenda2.SetActive(false);
            Mision1OpcionLeyenda3.SetActive(false);
            Mision1OpcionLeyenda4.SetActive(false);
            Mision1OpcionLeyenda5.SetActive(false);

            Mision2OpcionLeyenda1.SetActive(false);
            Mision2OpcionLeyenda2.SetActive(false);
            Mision2OpcionLeyenda3.SetActive(false);
            Mision2OpcionLeyenda4.SetActive(false);
            Mision2OpcionLeyenda5.SetActive(false);

            Mision3OpcionLeyenda1.SetActive(false);
            Mision3OpcionLeyenda2.SetActive(false);
            Mision3OpcionLeyenda3.SetActive(false);
            Mision3OpcionLeyenda4.SetActive(false);
            Mision3OpcionLeyenda5.SetActive(false);
			btnMision1.interactable=true;
			btnMision2.interactable=true;

            banderamision = true;
            m3 = true;

            if (banderaleyenda == 1)
            {
                Mision3OpcionLeyenda1.SetActive(true);
                fondonegrom3l1.SetActive(true);
                popupm3l1.SetActive(true);
                soundManager.playSound("Sound_OpenPopUp10", 1);
            }
            else
            {
                if (banderaleyenda == 2)
                {
                    Mision3OpcionLeyenda2.SetActive(true);
                    fondonegrom3l2.SetActive(true);
                    popupm3l2.SetActive(true);
                    soundManager.playSound("Sound_OpenPopUp10", 1);
                }
                else
                {
                    if (banderaleyenda == 3)
                    {
                        Mision3OpcionLeyenda3.SetActive(true);
                        fondonegrom3l3.SetActive(true);
                        popupm3l3.SetActive(true);
                        soundManager.playSound("Sound_OpenPopUp10", 1);
                    }

                    else
                    {
                        if (banderaleyenda == 4)
                        {
                            Mision3OpcionLeyenda4.SetActive(true);
                            fondonegrom3l4.SetActive(true);
                            popupm3l4.SetActive(true);
                            soundManager.playSound("Sound_OpenPopUp10", 1);
                        }
                        else
                        {
                            if (banderaleyenda == 5)
                            {
                                Mision3OpcionLeyenda5.SetActive(true);
                                fondonegrom3l5.SetActive(true);
                                popupm3l5.SetActive(true);
                                soundManager.playSound("Sound_OpenPopUp10", 1);
                            }
                        }
                    }
                }
            }


        });

        //boton aceptar de popup de mision3 leyenda1

        btnaceptarm3l1.onClick.AddListener(delegate
        {
            fondonegrom3l1.SetActive(false);
            popupm3l1.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l1.SetActive(true);
            globoexitom3l1.SetActive(false);
            globoerrorm3l1.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm3l1.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "La seguridad de los viajeros no depende de  la presencia de los petirrojos. ");

			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones--;
			
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);


            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton no aceptar de popup mision3leyenda1
        btnnom3l1.onClick.AddListener(delegate
        {
            ancianom3l1.SetActive(true);
            globoexitom3l1.SetActive(true);
            globoerrorm3l1.SetActive(false);
            label = globoexitom3l1.transform.GetChild(0).GetComponent<Text>();
				animateString(14f,
                "¡Una decisión correcta! La seguridad de los viajeros no depende de la presencia del petirrojo, por lo que su ausencia, probablemente se deba a una migración de la especie.");

            fondonegrom3l1.SetActive(false);
            popupm3l1.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            
			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones++;
				m3a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);

				Invoke("revisarpuntuacion", 15f);
            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
            //ancianom3l1.SetActive(false);
            //globoexitom3l1.SetActive(false);
            //globoerrorm3l1.SetActive(false);
        });


        //boton aceptar de popup de mision3 leyenda2
        btnaceptarm3l2.onClick.AddListener(delegate
        {
            fondonegrom3l2.SetActive(false);
            popupm3l2.SetActive(false);
            btnMision3.interactable = false;
            ancianom3l2.SetActive(true);
            globoexitom3l2.SetActive(false);
            globoerrorm3l2.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm3l2.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "No todos los elementos de una leyenda son ficticios, el bosque está muy oscuro y necesitamos iluminación de alguna parte. ");


            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            

			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);

            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton no aceptar de popup mision3 leyenda2
        btnnom3l2.onClick.AddListener(delegate
        {
            fondonegrom3l2.SetActive(false);
            popupm3l2.SetActive(false);
            btnMision3.interactable = false;
            ancianom3l2.SetActive(true);
            globoexitom3l2.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            globoerrorm3l2.SetActive(false);
            label = globoexitom3l2.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "Hiciste muy bien, el bosque es muy oscuro por lo que acampar allí sin iluminación no es seguro.");

            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            
			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones++;
				m3a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton aceptar de popup de mision3 leyenda3
        btnaceptarm3l3.onClick.AddListener(delegate
        {
            fondonegrom3l3.SetActive(false);
            popupm3l3.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l3.SetActive(true);
            globoexitom3l3.SetActive(false);
            soundManager.playSound("Sound_incorrecto19", 1);
            globoerrorm3l3.SetActive(true);
            label = globoerrorm3l3.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "Pedirle al Sol que se vaya es parte de la fantasía, la parte real, es que sin él nos moriríamos congelados. ");

            
			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion", 15f);

            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton no aceptar de popup mision3 leyenda3
        btnnom3l3.onClick.AddListener(delegate
        {
            fondonegrom3l3.SetActive(false);
            popupm3l3.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l3.SetActive(true);
            globoexitom3l3.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            globoerrorm3l3.SetActive(false);
            label = globoexitom3l3.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "¡Una decisión correcta! Pedirle al Sol que se vaya es parte de la fantasía en la leyenda, lo que sí es real es que sin el Sol nos moriríamos congelados. ");

            
			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones++;
				m3a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });


        //boton aceptar de popup de mision3 leyenda4
        btnaceptarm3l4.onClick.AddListener(delegate
        {
            fondonegrom3l4.SetActive(false);
            popupm3l4.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l4.SetActive(true);
            globoexitom3l4.SetActive(false);
            globoerrorm3l4.SetActive(true);
            soundManager.playSound("Sound_incorrecto19", 1);
            label = globoerrorm3l4.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "Adentrarte en el fuego es muy peligroso, la resistencia de la piel de coyote es parte de la fantasía en la leyenda, no es real.");

            
			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion",15f);

            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton no aceptar de popup mision3 leyenda4
        btnnom3l4.onClick.AddListener(delegate
        {
            fondonegrom3l4.SetActive(false);
            popupm3l4.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l4.SetActive(true);
            globoexitom3l4.SetActive(true);
            soundManager.playSound("Sound_correcto10", 1);
            globoerrorm3l4.SetActive(false);
            label = globoexitom3l4.transform.GetChild(0).GetComponent<Text>();
			animateString(14f,
                "¡Excelente decisión! Tu vida corría peligro al entrar al fuego y la piel de coyote no te ofrecía protección, es parte de la fantasía. ");

            

			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones++;
				m3a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            //Invoke("revisarpuntuacion", 0f);

				Invoke("revisarpuntuacion", 15f);
            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton aceptar de popup de mision3 leyenda5
        btnaceptarm3l5.onClick.AddListener(delegate
        {
            fondonegrom3l5.SetActive(false);
            popupm3l5.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l5.SetActive(true);
            globoexitom3l5.SetActive(true);
            globoerrorm3l5.SetActive(false);
            soundManager.playSound("Sound_correcto10", 1);
            label = globoexitom3l5.transform.GetChild(0).GetComponent<Text>();
            animateString(14f, "Contestar una pregunta es una forma de atención a las personas. ¡Bien hecho!");

            

			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones++;
				m3a = true;
            //corazonesreto++;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            // Invoke("revisarpuntuacion", 0f);
				Invoke("revisarpuntuacion", 15f);
            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });

        //boton no aceptar de popup mision3 leyenda5
        btnnom3l5.onClick.AddListener(delegate
        {
            fondonegrom3l5.SetActive(false);
            popupm3l5.SetActive(false);
            btnMision3.interactable = false;
            btnMision1.gameObject.SetActive(true);
            btnMision2.gameObject.SetActive(true);
            btnMision3.gameObject.SetActive(true);
            ancianom3l5.SetActive(true);
            globoexitom3l5.SetActive(false);
            soundManager.playSound("Sound_incorrecto19", 1);
            globoerrorm3l5.SetActive(true);
            label = globoerrorm3l5.transform.GetChild(0).GetComponent<Text>();
            animateString(14f,
                " En la leyenda es recomendable contestar y en la realidad contestar una pregunta es una muestra de cortesía.");

            
			btnMision1.interactable = false;
			btnMision2.interactable = false;
			numcorazones--;
            //corazonesreto--;
            //playSound("GanarEstrella", audios[1]);
            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
            Invoke("revisarpuntuacion",15f);

            //a los 6 segundos sale un popup final
            Invoke("mostrarpopfinal", 17f);
            btnMision1.interactable = false;
            btnMision2.interactable = false;
            btnMision3.interactable = false;
        });


        btnaceptarfinal.onClick.AddListener(delegate
        {
            Reto1Obj.SetActive(false);
            Reto2Obj.SetActive(true);
        });
    }

    // Update is called once per frame
    private void Update()
    {
        if (Reto1Obj.activeInHierarchy && !flag)
        {
            
            flag = true;
        }

        if (!animationStarted)
            return;
        timeAnimation -= Time.deltaTime;
        if (timeAnimation <= 0)
            timeAnimation = 0;

        label.text = text.Substring(0, (int) ((timeTotal - timeAnimation)*text.Length/timeTotal));
    }

    private void mostrarleyenda()
    {
        
            if (rand == 1)
            {
                Leyenda1.SetActive(true);
                banderaleyenda = 1;
            }
            else
            {
                if (rand == 2)
                {
                    Leyenda2.SetActive(true);
                    banderaleyenda = 2;
                }
                else
                {
                    if (rand == 3)
                    {
                        Leyenda3.SetActive(true);
                        banderaleyenda = 3;
                    }
                    else
                    {
                        if (rand == 4)
                        {
                            Leyenda4.SetActive(true);
                            banderaleyenda = 4;
                        }
                        else
                        {
                            if (rand == 5)
                            {
                                Leyenda5.SetActive(true);
                                banderaleyenda = 5;
                            }

                        }
                    }
                }
            }
        
    }

    private void mostrarpopfinal()
    {
        //mostrar pop final
		if (m1 && m2 && m3 && numcorazones>4)
        {
            popupfinal.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp10", 1);
            //corazonesreto=1;
			PlayerPrefs.GetInt ("corazonestest");
			print(numcorazones);
			//PlayerPrefs.SetInt ("corazonesODA2_ODA2", numcorazones);
			PlayerPrefs.SetInt ("corazonestest", numcorazones);
			//PlayerPrefs.SetInt ("corazonesODA2_ODA2", "test");
            //corazonesreto = PlayerPrefs.GetInt("corazonesODA2_ODA2");
        }
       else
        {
            //popupcr.SetActive(true);
			//mandar llamar función revisar misiones
			revisarretos();
            //soundManager.playSound("Sound_OpenPopUp10", 1);
        }
    }

    private void cerrarIndex()
    {
		//inicializar variable para los corazones
		//PlayerPrefs.SetInt("corazonesODA2_ODA2", 0);
		numcorazones=0;
		PlayerPrefs.SetInt ("corazonestest", numcorazones);
		Index.SetActive(false); //apagar el objeto en escena
        Portada.SetActive(true); //prender el objeto en escena
        //prender el menú
        menu.SetActive(true);

        //DropDownMenuCamvas.SetActive(true);
    }


    public void animateString(float time, string t)
    {
        text = t;
        timeAnimation = time;
        timeTotal = timeAnimation;
        animationStarted = true;
    }


    private void seleccionarReto(Button bot)
    {
        //Clon = Instantiate<GameObject>(this.gameObject);
		Clon = Instantiate(this.gameObject);

        Clon.SetActive(false);

        Portada.SetActive(false);
        numcorazones = 2;
        //corazonesreto = PlayerPrefs.GetInt("corazonesODA2_ODA2");
        corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
        
		if (bot.name == "btnReto1")
        {
            menu.SetActive(true);
            Reto1Obj.SetActive(true);
            Pantalla2.SetActive(true);
            soundManager.playSound("Fondo_Cuervo", 0);
            fondonegro.SetActive(true);
            popupp2.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp10", 1);

        }
        else if (bot.name == "btnReto2")
        {
            menu.SetActive(true);
            Reto2Obj.SetActive(true);
            soundManager.playSound("Fondo_lobo3", 0);
        }
        else if (bot.name == "btnReto3")
        {
            menu.SetActive(true);
            Reto3Obj.SetActive(true);
            soundManager.playSound("Sound_SonidoFondo2", 0);
        }
        else if (bot.name == "btnReto4")
        {
            menu.SetActive(true);
            Reto4Obj.SetActive(true);
            soundManager.playSound("Fondo_piano", 0);
        }
        else if (bot.name == "btnRetoFinal")
        {
            RetoFinalObj.SetActive(true);
            StartCoroutine(GetComponent<RetoFinal_ODA02>().secuenciaEventos("iniciarEvaluacion", 0.0f));
            soundManager.playSound("Sound_Fondoevaluacion", 0);
        }
    }

    private void revisarpuntuacion()
    {
        if (numcorazones <= 0)
        {
            //mostrar pop up perder y reiniciar reto
            popuppierdevidas.SetActive(true);
            soundManager.playSound("Sound_PerderVida10", 1);
        }

        if (numcorazones >= 1 && numcorazones < 5 && m1 && m2 && m3)
        {
            //mostrar pop up perder y reiniciar reto
            //reiniciar

            // pasar al reto 2

            popupfinal.SetActive(true);
                soundManager.playSound("Sound_OpenPopUp10", 1);
                //corazonesreto=1;
                PlayerPrefs.GetInt("corazonestest");
                print(numcorazones);
                //PlayerPrefs.SetInt ("corazonesODA2_ODA2", numcorazones);
                PlayerPrefs.SetInt("corazonestest", numcorazones);
                //PlayerPrefs.SetInt ("corazonesODA2_ODA2", "test");
                //corazonesreto = PlayerPrefs.GetInt("corazonesODA2_ODA2");
            }


        


        /*	//ver si falta la m1 de acertar
            if (numcorazones >= 1 && numcorazones< 5 && m1a )
            {
                //mostrar pop up perder y reiniciar reto
                //reiniciar

                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;

            }
            if (numcorazones >= 1 && numcorazones< 5 && m2a )
            {
                //mostrar pop up perder y reiniciar reto
                //reiniciar
                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;

                btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;

            }


            if (numcorazones >= 1 && numcorazones< 5 && m3a )
            {
                //mostrar pop up perder y reiniciar reto
                //reiniciar
                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;


            }


            //new
            //si no ha hecho la mision3
            if (m1a==true && m2a == true && m3a==false && numcorazones >= 1 && numcorazones< 5) {
                btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;
                //btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                //btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;

            }

            //si no ha hecho la mision2
            if (m1a == true && m2a == false && m3a == true && numcorazones >= 1 && numcorazones< 5)
            {
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                //btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
                //btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;
            }

            //si no ha hecho la mision1
            if (m1a == false && m2a == true && m3a == true && numcorazones >= 1 && numcorazones< 5)
            {

                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                //btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                //btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
            }


            //si solo ha hecho la mision 1

            if (m1a == true && m2a == false && m3a == false && numcorazones >= 1 && numcorazones< 5)
            {

                //btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                //btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;
            }

            //si solo ha hecho la mision2
            if (m1a == false && m2a == true && m3a == false && numcorazones >= 1 && numcorazones< 5)
            {

                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                //btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                //btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;
            }
            //si solo ha hecho la mision 3

            if (m1a== false && m2a == false && m3a == true && numcorazones >= 1 && numcorazones< 5)
            {

                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                //btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
            }

            //si ya completò las 3 y tiene 4 corazones
            if (m1a== true && m2a == true && m3a == true && numcorazones >= 1 && numcorazones< 5)
            {


                btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;
                btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
                    popupfinal.SetActive(true);
                    soundManager.playSound("Sound_OpenPopUp10", 1);
                    //corazonesreto=1;
                    PlayerPrefs.GetInt ("corazonestest");
                    print(numcorazones);
                    //PlayerPrefs.SetInt ("corazonesODA2_ODA2", numcorazones);
                    PlayerPrefs.SetInt ("corazonestest", numcorazones);
                    //PlayerPrefs.SetInt ("corazonesODA2_ODA2", "test");
                    //corazonesreto = PlayerPrefs.GetInt("corazonesODA2_ODA2");



            }*/



    }


	private void revisarretos()
	//boton para cuando faltaron misiones de cumplir
	/* btnaceptarcr.onClick.AddListener(delegate*/
        {
            //popupcr.SetActive(false);

            //desactivar todos los objetos
            //desactivar todas las misiones
            Mision1OpcionLeyenda1.SetActive(false);
            popupm1l1.SetActive(false);
            ancianom1l1.SetActive(false);
            globoexitom1l1.SetActive(false);
            globoerrorm1l1.SetActive(false);

            Mision1OpcionLeyenda2.SetActive(false);
            popupm1l2.SetActive(false);
            ancianom1l2.SetActive(false);
            globoexitom1l2.SetActive(false);
            globoerrorm1l2.SetActive(false);

            Mision1OpcionLeyenda3.SetActive(false);
            popupm1l3.SetActive(false);
            ancianom1l3.SetActive(false);
            globoexitom1l3.SetActive(false);
            globoerrorm1l3.SetActive(false);

            Mision1OpcionLeyenda4.SetActive(false);
            popupm1l4.SetActive(false);
            ancianom1l4.SetActive(false);
            globoexitom1l4.SetActive(false);
            globoerrorm1l4.SetActive(false);

            Mision1OpcionLeyenda5.SetActive(false);
            popupm1l5.SetActive(false);
            ancianom1l5.SetActive(false);
            globoexitom1l5.SetActive(false);
            globoerrorm1l5.SetActive(false);

            Mision2OpcionLeyenda1.SetActive(false);
            popupm2l1.SetActive(false);
            ancianom2l1.SetActive(false);
            globoexitom2l1.SetActive(false);
            globoerrorm2l1.SetActive(false);


            Mision2OpcionLeyenda2.SetActive(false);
            popupm2l2.SetActive(false);
            ancianom2l2.SetActive(false);
            globoexitom2l2.SetActive(false);
            globoerrorm2l2.SetActive(false);

            Mision2OpcionLeyenda3.SetActive(false);
            popupm2l3.SetActive(false);
            ancianom2l3.SetActive(false);
            globoexitom2l3.SetActive(false);
            globoerrorm2l3.SetActive(false);

            Mision2OpcionLeyenda4.SetActive(false);
            popupm2l4.SetActive(false);
            ancianom2l4.SetActive(false);
            globoexitom2l4.SetActive(false);
            globoerrorm2l4.SetActive(false);

            Mision2OpcionLeyenda5.SetActive(false);
            popupm2l5.SetActive(false);
            ancianom2l5.SetActive(false);
            globoexitom2l5.SetActive(false);
            globoerrorm2l5.SetActive(false);

            Mision3OpcionLeyenda1.SetActive(false);
            popupm3l1.SetActive(false);
            ancianom3l1.SetActive(false);
            globoexitom3l1.SetActive(false);
            globoerrorm3l1.SetActive(false);

            Mision3OpcionLeyenda2.SetActive(false);
            popupm3l2.SetActive(false);
            ancianom3l2.SetActive(false);
            globoexitom3l2.SetActive(false);
            globoerrorm3l2.SetActive(false);

            Mision3OpcionLeyenda3.SetActive(false);
            popupm3l3.SetActive(false);
            ancianom3l3.SetActive(false);
            globoexitom3l3.SetActive(false);
            globoerrorm3l3.SetActive(false);

            Mision3OpcionLeyenda4.SetActive(false);
            popupm3l4.SetActive(false);
            ancianom3l4.SetActive(false);
            globoexitom3l4.SetActive(false);
            globoerrorm3l4.SetActive(false);

            Mision3OpcionLeyenda5.SetActive(false);
            popupm3l5.SetActive(false);
            ancianom3l5.SetActive(false);
            globoexitom3l5.SetActive(false);
            globoerrorm3l5.SetActive(false);
           
            
            //si no ha hecho la mision3
            if (m1==true && m2 == true && m3==false) {
                btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;
                //btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                //btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;

            }

            //si no ha hecho la mision2
            if (m1 == true && m2 == false && m3 == true)
            {
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                //btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
                //btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;
            }

            //si no ha hecho la mision1
            if (m1 == false && m2 == true && m3 == true)
            {
                            
                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                //btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                //btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
            }


            //si solo ha hecho la mision 1
            
            if (m1 == true && m2 == false && m3 == false)
            {

                //btnMision1.gameObject.SetActive(false);
                btnMision1.interactable = false;
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                //btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;
            }

            //si solo ha hecho la mision2
            if (m1 == false && m2 == true && m3 == false)
            {

                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                //btnMision2.gameObject.SetActive(false);
                btnMision2.interactable = false;
                //btnMision3.gameObject.SetActive(true);
                btnMision3.interactable = true;
            }
            //si solo ha hecho la mision 3

            if (m1 == false && m2 == false && m3 == true)
            {

                btnMision1.gameObject.SetActive(true);
                btnMision1.interactable = true;
                btnMision2.gameObject.SetActive(true);
                btnMision2.interactable = true;
                //btnMision3.gameObject.SetActive(false);
                btnMision3.interactable = false;
            }


        }
	

}