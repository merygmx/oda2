﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Reto4 : MonoBehaviour
{
    Texture2D texture1;
    private static AndroidJavaObject _activity;
    private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";
    private Button btnaceptarr4;
    private Button btnguardar1;
    private Button btnguardar2;
    private Button btnguardar3;
    private Button btnguardar4;
	private int numcorazones;
    //private Button btnguardar5;
    private Button btnnaturaleza;
    private Button btnlocalidad;
    private Button btnheroes;
    private Button btnvidamuerte;
    private Button btnaceptarg;
    private Button btnsi;
    private Button btnno;
    private GameObject Reto4Obj;
    private GameObject EvaluacionFinalObj;
    private GameObject fondo;
    private GameObject popupr4;
    private GameObject fondo2;
    private GameObject fondo2a;
    private GameObject pantallafuturista;
    private GameObject machotelocalidadycost;
    private GameObject machoteheroes;
    private GameObject z5;
    private GameObject z6;
    private GameObject z7;
    private GameObject z8;
    private GameObject machotenaturaleza;
    private GameObject z9;
    private GameObject z10;
    private GameObject z11;
    private GameObject z12;
    private GameObject machotelavida;
    private GameObject z13;
    private GameObject z14;
    private GameObject z15;
    private GameObject z16;
    private GameObject popupguardar;
    private GameObject popupguardarerror;
    private SoundManager soundManager;
    private InputField texttitulo1;
    private InputField textdesarrollo1;
    private InputField textinicio1;
    private InputField textfinal1;
    private InputField texttitulo2;
    private InputField textdesarrollo2;
    private InputField textinicio2;
    private InputField textfinal2;
    private InputField texttitulo3;
    private InputField textdesarrollo3;
    private InputField textinicio3;
    private InputField textfinal3;
    private InputField texttitulo4;
    private InputField textdesarrollo4;
    private InputField textinicio4;
    private InputField textfinal4;

    // Use this for initialization
    private void Start()
    {
        Reto4Obj = transform.FindChild("Reto4").gameObject;
        Reto4Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
        EvaluacionFinalObj = transform.FindChild("RetoFinal").gameObject;
        fondo = Reto4Obj.transform.GetChild(0).FindChild("fondo").gameObject;
        popupr4 = Reto4Obj.transform.GetChild(0).FindChild("popupr4").gameObject;
        btnaceptarr4 = popupr4.transform.FindChild("btnaceptar").GetComponent<Button>();
        fondo2 = Reto4Obj.transform.GetChild(0).FindChild("fondo2").gameObject;
        fondo2a = fondo2.transform.FindChild("fondo2-a").gameObject;
        pantallafuturista = fondo2.transform.FindChild("pantallafuturista").gameObject;
        btnnaturaleza = fondo2.transform.FindChild("btnnaturaleza").GetComponent<Button>();
        btnlocalidad = fondo2.transform.FindChild("btnlocalidad").GetComponent<Button>();
        btnheroes = fondo2.transform.FindChild("btnheroes").GetComponent<Button>();
        btnvidamuerte = fondo2.transform.FindChild("btnvidamuerte").GetComponent<Button>();
        soundManager = transform.GetComponent<SoundManager>();
        soundManager.playSound("Fondo_piano", 0);

        machotenaturaleza = pantallafuturista.transform.FindChild("machotenaturaleza").gameObject;
        btnguardar1 = machotenaturaleza.transform.FindChild("btnguardar").GetComponent<Button>();
        texttitulo1 = machotenaturaleza.transform.FindChild("texttitulo").GetComponent<InputField>();
        textdesarrollo1 = machotenaturaleza.transform.FindChild("textdesarrollo").GetComponent<InputField>();
        textinicio1 = machotenaturaleza.transform.FindChild("textinicio").GetComponent<InputField>();
        textfinal1 = machotenaturaleza.transform.FindChild("textfinal").GetComponent<InputField>();
        //textt = machotenaturaleza.transform.FindChild("texttitulo").GetComponent<InputField>();

        machoteheroes = pantallafuturista.transform.FindChild("machoteheroes").gameObject;
        btnguardar2 = machoteheroes.transform.FindChild("btnguardar").GetComponent<Button>();
        texttitulo2 = machoteheroes.transform.FindChild("texttitulo").GetComponent<InputField>();
        textdesarrollo2 = machoteheroes.transform.FindChild("textdesarrollo").GetComponent<InputField>();
        textinicio2 = machoteheroes.transform.FindChild("textinicio").GetComponent<InputField>();
        textfinal2 = machoteheroes.transform.FindChild("textfinal").GetComponent<InputField>();


        machotelocalidadycost = pantallafuturista.transform.FindChild("machotelocalidadycost").gameObject;
        btnguardar3 = machotelocalidadycost.transform.FindChild("btnguardar").GetComponent<Button>();
        texttitulo3 = machotelocalidadycost.transform.FindChild("texttitulo").GetComponent<InputField>();
        textdesarrollo3 = machotelocalidadycost.transform.FindChild("textdesarrollo").GetComponent<InputField>();
        textinicio3 = machotelocalidadycost.transform.FindChild("textinicio").GetComponent<InputField>();
        textfinal3 = machotelocalidadycost.transform.FindChild("textfinal").GetComponent<InputField>();


        machotelavida = pantallafuturista.transform.FindChild("machotelavida").gameObject;
        btnguardar4 = machotelavida.transform.FindChild("btnguardar").GetComponent<Button>();
        texttitulo4 = machotelavida.transform.FindChild("texttitulo").GetComponent<InputField>();
        textdesarrollo4 = machotelavida.transform.FindChild("textdesarrollo").GetComponent<InputField>();
        textinicio4 = machotelavida.transform.FindChild("textinicio").GetComponent<InputField>();
        textfinal4 = machotelavida.transform.FindChild("textfinal").GetComponent<InputField>();


        popupguardar = pantallafuturista.transform.FindChild("popupguardar").gameObject;
        btnaceptarg = popupguardar.transform.FindChild("btnaceptar").GetComponent<Button>();
        popupguardarerror = pantallafuturista.transform.FindChild("popupguardarerror").gameObject;
        btnsi = popupguardarerror.transform.FindChild("btnsi").GetComponent<Button>();
        btnno = popupguardarerror.transform.FindChild("btnno").GetComponent<Button>();


        Invoke("iniciarreto", 2f);

        btnaceptarr4.onClick.AddListener(delegate
        {
            popupr4.SetActive(false);
            fondo.SetActive(false);
            fondo2.SetActive(true);
			numcorazones=PlayerPrefs.GetInt("corazonestest");
            //pantallafuturista.SetActive(true);
				print ("Reto 4: " + numcorazones);
        });


        btnnaturaleza.onClick.AddListener(delegate
        {
            fondo2a.SetActive(true);
            pantallafuturista.SetActive(true);
            machotenaturaleza.SetActive(true);
            texttitulo1.Select();


            //fondo2.SetActive(true);
            //pantallafuturista.SetActive(true);
        });

        btnlocalidad.onClick.AddListener(delegate
        {
            fondo2a.SetActive(true);
            pantallafuturista.SetActive(true);
            machotelocalidadycost.SetActive(true);
            texttitulo3.Select();
            //fondo2.SetActive(true);
            //pantallafuturista.SetActive(true);

        });


        btnheroes.onClick.AddListener(delegate
        {
            fondo2a.SetActive(true);
            pantallafuturista.SetActive(true);
            machoteheroes.SetActive(true);
            texttitulo2.Select();
            //fondo2.SetActive(true);
            //pantallafuturista.SetActive(true);

        });


        btnvidamuerte.onClick.AddListener(delegate
        {
            fondo2a.SetActive(true);
            pantallafuturista.SetActive(true);
            machotelavida.SetActive(true);
            texttitulo4.Select();
            //fondo2.SetActive(true);
            //pantallafuturista.SetActive(true);

        });


        btnguardar1.onClick.AddListener(delegate
        {
            //verificar si llenó todos los inputtext
            if ((texttitulo1.text == "") || (textdesarrollo1.text == "") || (textinicio1.text == "") ||
                (textfinal1.text == ""))
            {
                soundManager.playSound("Sound_OpenPopUp10", 1);
                popupguardarerror.SetActive(true);
				btnguardar1.interactable= false;
                btnguardar1.gameObject.SetActive(false);

            }
            else
            {
                btnguardar1.gameObject.SetActive(false);
                btnguardar2.gameObject.SetActive(false);
                btnguardar3.gameObject.SetActive(false);
                btnguardar4.gameObject.SetActive(false);
                //guardar pantalla
                StartCoroutine("ScreenshotEncode");
            }
        });

        btnguardar2.onClick.AddListener(delegate
        {
            //verificar si llenó todos los inputtext
            if ((texttitulo2.text == "") || (textdesarrollo2.text == "") || (textinicio2.text == "") ||
                (textfinal2.text == ""))
            {
                soundManager.playSound("Sound_OpenPopUp10", 1);
                popupguardarerror.SetActive(true);
				btnguardar2.interactable= false;
                btnguardar2.gameObject.SetActive(false);
                //soundManager.playSound ("Fondo piano mp3",0);
            }
            else
            {
                btnguardar1.gameObject.SetActive(false);
                btnguardar2.gameObject.SetActive(false);
                btnguardar3.gameObject.SetActive(false);
                btnguardar4.gameObject.SetActive(false);
                //guardar pantala
                StartCoroutine("ScreenshotEncode");
            }
        });

        btnguardar3.onClick.AddListener(delegate
        {
            //verificar si llenó todos los inputtext
            if ((texttitulo3.text == "") || (textdesarrollo3.text == "") || (textinicio3.text == "") ||
                (textfinal3.text == ""))
            {
                soundManager.playSound("Sound_OpenPopUp10", 1);
                popupguardarerror.SetActive(true);
				btnguardar3.interactable= false;
                btnguardar3.gameObject.SetActive(false);
            }
            else
            {
                btnguardar1.gameObject.SetActive(false);
                btnguardar2.gameObject.SetActive(false);
                btnguardar3.gameObject.SetActive(false);
                btnguardar4.gameObject.SetActive(false);
                //guardar pantala
                StartCoroutine("ScreenshotEncode");
            }
        });

        btnguardar4.onClick.AddListener(delegate
        {
            //verificar si llenó todos los inputtext
            if ((texttitulo4.text == "") || (textdesarrollo4.text == "") || (textinicio4.text == "") ||
                (textfinal4.text == ""))
            {
                soundManager.playSound("Sound_OpenPopUp10", 1);
                popupguardarerror.SetActive(true);
				btnguardar4.interactable= false;
                btnguardar4.gameObject.SetActive(false);
            }
            else
            {
                btnguardar1.gameObject.SetActive(false);
                btnguardar2.gameObject.SetActive(false);
                btnguardar3.gameObject.SetActive(false);
                btnguardar4.gameObject.SetActive(false);
                //guardar pantalla
                StartCoroutine("ScreenshotEncode");
            }
        });


        btnaceptarg.onClick.AddListener(delegate
        {
			print("Mensaje de test a 0");
		    PlayerPrefs.SetInt("corazonestest",0);
			EvaluacionFinalObj.SetActive(true);
            //RetoFinalObj.SetActive (true);
            StartCoroutine(GetComponent<RetoFinal_ODA02>().secuenciaEventos("iniciarEvaluacion", 0.0f));
            soundManager.playSound("Sound_Fondoevaluacion", 0);
            //soundManager.playSound ("Sound_Fondoevaluacion",0);
            Reto4Obj.SetActive(false);

            //fondo2.SetActive(true);
            //pantallafuturista.SetActive(true);
        });


        btnsi.onClick.AddListener(delegate
        {
            //apagar popup 
            popupguardarerror.SetActive(false);

            //apagar botones guardar
            btnguardar1.gameObject.SetActive(false);
            btnguardar2.gameObject.SetActive(false);
            btnguardar3.gameObject.SetActive(false);
            btnguardar4.gameObject.SetActive(false);

            //guardar pantalla
            StartCoroutine("ScreenshotEncode");
        });

        btnno.onClick.AddListener(delegate { 
			popupguardarerror.SetActive(false); 

			btnguardar1.interactable= true;
            btnguardar1.gameObject.SetActive(true);
            btnguardar2.interactable= true;
            btnguardar2.gameObject.SetActive(true);
            btnguardar3.interactable= true;
            btnguardar3.gameObject.SetActive(true);
            btnguardar4.interactable= true;
            btnguardar4.gameObject.SetActive(true);
            //btnguardar5.gameObject.SetActive(true);
        });

    }

    // Update is called once per frame
    private void Update()
    {
    }

    

    public static string SaveImageToGallery(Texture2D texture2D, string title, string description)
    {
        using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass))
        {
            using (var cr = Activity.Call<AndroidJavaObject>("getContentResolver"))
            {
                var image = Texture2DToAndroidBitmap(texture2D);
                var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
                return imageUrl;
            }
        }
    }

    public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D)
    {
        byte[] encoded = texture2D.EncodeToPNG();
        using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory"))
        {
            return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
        }
    }

    public static AndroidJavaObject Activity
    {
        get
        {
            if (_activity == null)
            {

                var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
               

                if (unityPlayer == null)
                {
                    throw new System.Exception("Could not get class com.unity3d.player.UnityPlayer.");
                }

                _activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                if (_activity == null)
                {
                    throw new System.Exception("Could not get class current activity from UnityPlayer.");
                }


            }
            return _activity;
        }
    }


    private IEnumerator ScreenshotEncode()
    {
        /*  yield return new WaitForEndOfFrame();
          var datacion = DateTime.Now.ToFileTime().ToString();
          Application.CaptureScreenshot(datacion + ".png");*/

        yield return new WaitForEndOfFrame();
        texture1 = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture1.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture1.Apply();
        SaveImageToGallery(texture1, "CapturaOda2", "");
        soundManager.playSound("Sound_OpenPopUp10", 1);
        popupguardar.SetActive(true);

    }

    private void iniciarreto()
    {
        soundManager.playSound("Sound_OpenPopUp10", 1);
        popupr4.SetActive(true);


    }


}