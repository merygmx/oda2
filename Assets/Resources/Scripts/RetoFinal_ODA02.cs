﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetoFinal_ODA02 : MonoBehaviour
{
   public  int iniciaps = 0;
    //Colores preguntas
    private readonly Color answerSettedColor = new Color32(14, 209, 242, 255);
    private readonly Color defaultAnswerColor = new Color32(9, 74, 139, 255);
    
    //Colores preguntas calificadas
    private readonly Color incorrecta = new Color32 (255, 51, 51, 255);
	private readonly Color correcta = new Color32 (83, 186, 66, 255);

    //popUpsButtons
    private Button btnAtras;
    private Button btnAceptar;
    private Button btnSalir;
    private readonly List<Button> btnRespuestasMultiple = new List<Button>();
    private readonly List<Button> btnRespuestasSimple = new List<Button>();
    //private readonly Color defaultAnswerColor = new Color32(9, 74, 139, 255);

    private bool iniciado;

    //popUps
    private readonly List<GameObject> popUps = new List<GameObject>();
    private int preguntaActual;
	private GameObject palom;

    //Variables de evaluacion
    private List<Question> preguntas = new List<Question>();
    private List<Answer> respuestas = new List<Answer>();

    //Repuestas GameObject
    private GameObject respuestasSimple;
    private GameObject respuestasMultiple;

    //Main gameobject
	private GameObject RetoFinal;

    //Scripts ayuda
    private SoundManager soundScript;

    private Text txtCalificacion;
    private Text txtRespuesta;
	private Text txtRespuesta2;
	private Text indexp;


    //popUpsText
    private Text txtPregunta;
    private readonly List<Text> txtRespuestasMultiple = new List<Text>();
    private readonly List<Text> txtRespuestasSimple = new List<Text>();


    // ------------------------Genericos Unity----------------------
    private void Start()
    {
        RetoFinal = transform.FindChild("RetoFinal").gameObject;
		RetoFinal.transform.GetComponent<Canvas>().worldCamera = Camera.main;

        //Scripts de Ayuda
        soundScript = transform.GetComponent<SoundManager>();

        //popUps
        popUps.Add(RetoFinal.transform.FindChild("popUpPregunta").gameObject);
		palom = popUps[0].transform.FindChild ("palom").gameObject;
        popUps.Add(RetoFinal.transform.FindChild("popUpResultados").gameObject);

        //popUps Buttons
        btnAtras = popUps[0].transform.FindChild("btnAnterior").GetComponent<Button>();
        btnAceptar = popUps[0].transform.FindChild("btnAceptar").GetComponent<Button>();

        btnSalir = popUps[1].transform.FindChild("btnSalir").GetComponent<Button>();

        //Preguntas simples
        respuestasSimple = popUps[0].transform.FindChild("respuestasSimple").gameObject;
        btnRespuestasSimple.Add(respuestasSimple.transform.FindChild("btnRespuesta1").GetComponent<Button>());
        btnRespuestasSimple.Add(respuestasSimple.transform.FindChild("btnRespuesta2").GetComponent<Button>());
        btnRespuestasSimple.Add(respuestasSimple.transform.FindChild("btnRespuesta3").GetComponent<Button>());
        btnRespuestasSimple.Add(respuestasSimple.transform.FindChild("btnRespuesta4").GetComponent<Button>());

        txtRespuestasSimple.Add(btnRespuestasSimple[0].transform.GetChild(0).GetComponent<Text>());
        txtRespuestasSimple.Add(btnRespuestasSimple[1].transform.GetChild(0).GetComponent<Text>());
        txtRespuestasSimple.Add(btnRespuestasSimple[2].transform.GetChild(0).GetComponent<Text>());
        txtRespuestasSimple.Add(btnRespuestasSimple[3].transform.GetChild(0).GetComponent<Text>());

        //Preguntas Multiple
        respuestasMultiple = popUps[0].transform.FindChild("respuestasMultiple").gameObject;
        btnRespuestasMultiple.Add(respuestasMultiple.transform.FindChild("btnRespuesta1").GetComponent<Button>());
        btnRespuestasMultiple.Add(respuestasMultiple.transform.FindChild("btnRespuesta2").GetComponent<Button>());
        btnRespuestasMultiple.Add(respuestasMultiple.transform.FindChild("btnRespuesta3").GetComponent<Button>());
        
		txtRespuestasMultiple.Add(btnRespuestasMultiple[0].transform.GetChild(0).GetComponent<Text>());
        txtRespuestasMultiple.Add(btnRespuestasMultiple[1].transform.GetChild(0).GetComponent<Text>());
        txtRespuestasMultiple.Add(btnRespuestasMultiple[2].transform.GetChild(0).GetComponent<Text>());
        

        //popUps Text
        txtPregunta = popUps[0].transform.FindChild("txtPregunta").GetComponent<Text>();

        txtCalificacion = popUps[1].transform.FindChild("txtCalificacion").GetComponent<Text>();
        txtRespuesta = popUps[1].transform.FindChild("resultadoPregunta").GetComponent<Text>();
		txtRespuesta2 = popUps[1].transform.FindChild("resultadoPregunta2").GetComponent<Text>();
		indexp= popUps[1].transform.FindChild("indexp").GetComponent<Text>();


        //Funciones Botones
        btnAtras.onClick.AddListener(delegate { regresar(); });
        btnAceptar.onClick.AddListener(delegate { avanzar(); });

        btnSalir.onClick.AddListener(delegate
        {
            limpiarResultados();
            GetComponent<Reto1Oda2>().Clon.SetActive(true);
            Destroy(gameObject);
        });

        
        btnRespuestasSimple[0].onClick.AddListener(delegate { seleccionarRespuesta(0); });

        btnRespuestasSimple[1].onClick.AddListener(delegate { seleccionarRespuesta(1); });

        btnRespuestasSimple[2].onClick.AddListener(delegate { seleccionarRespuesta(2); });

        btnRespuestasSimple[3].onClick.AddListener(delegate { seleccionarRespuesta(3); });

        btnRespuestasMultiple[0].onClick.AddListener(delegate { seleccionarRespuesta(0); });

        btnRespuestasMultiple[1].onClick.AddListener(delegate { seleccionarRespuesta(1); });

        btnRespuestasMultiple[2].onClick.AddListener(delegate { seleccionarRespuesta(2); });

        
    }

    private void Update()
    {
        if (RetoFinal.activeSelf && !iniciado)
        {
            iniciado = true;
            StartCoroutine(secuenciaEventos("iniciarEvaluacion", 0.0f));



        }



    }

    // ------------------------Genericos Retos----------------------

    //Funciones secuencia
    public IEnumerator secuenciaEventos(string secuencia, float tiempoEspera)
    {
        yield return new WaitForSeconds(tiempoEspera);
        switch (secuencia)
        {
            case "iniciarEvaluacion":
                soundScript.playSound("Sound_Fondoevaluacion", 0);
                crearCuestionario();
                seleccionarPreguntasRandom(5);
                preguntaActual = 0;
                popUps[0].SetActive(true);
                popUps[1].SetActive(false);
                limpiarResultados();
                mostrarPregunta();
                break;
            case "evaluar":
                mostrarResultados();
                break;
            case "salir":
                limpiarResultados();
                iniciado = false;
                GetComponent<Reto1Oda2>().Clon.SetActive(true);
                Destroy(gameObject);
                break;
        }
    }

    // ------------------------Funciones UI------------------------------
    private void mostrarPregunta()
    {
        txtPregunta.text = "" + (preguntaActual + 1) + ". " + respuestas[preguntaActual].GetQuestion().GetQuestionText();
        
        if (respuestas[preguntaActual].GetQuestion().GetQuestionType() == "single")
        {
            respuestasMultiple.SetActive(false);
			palom.SetActive (false);
            respuestasSimple.SetActive(true);

            txtRespuestasSimple[0].text = "" + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[0];
            txtRespuestasSimple[1].text = "a) " + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[1];
            txtRespuestasSimple[2].text = "b) " + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[2];
            txtRespuestasSimple[3].text = "c) " + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[3];


            colorearRespuesta ();
        }
		else if (respuestas[preguntaActual].GetQuestion().GetQuestionType() == "multiplee" || respuestas[preguntaActual].GetQuestion().GetQuestionType() == "multiple1"  )
        {
            
			if (respuestas[preguntaActual].GetQuestion().GetQuestionType() == "multiple1"){
				palom.SetActive (true);

			}
			if (respuestas[preguntaActual].GetQuestion().GetQuestionType() == "multiplee"){
				palom.SetActive (false);

			}

			respuestasMultiple.SetActive(true);
            respuestasSimple.SetActive(false);
            for (var index = 0; index < txtRespuestasMultiple.Count; index++)
                txtRespuestasMultiple[index].text = "" + respuestas[preguntaActual].GetQuestion().GetAnserTexts()[index];

            
			colorearRespuesta ();

        }


        if (preguntaActual <= 0)
        {
            //btnAtras.gameObject.SetActive(false);
			btnAtras.interactable = false;
			btnAtras.transform.GetChild(0).GetComponent<Text>().text = "ANTERIOR";
            //btnAceptar.transform.localPosition = btnAceptar.transform.localPosition + Vector3.left*216;
			btnAceptar.transform.localPosition = new Vector3(216, btnAceptar.transform.localPosition.y,
				btnAceptar.transform.localPosition.z);
        }
        else
        {
			btnAtras.interactable= true;
			btnAtras.gameObject.SetActive(true);
            btnAtras.transform.GetChild(0).GetComponent<Text>().text = "ANTERIOR";

            btnAceptar.transform.localPosition = new Vector3(216, btnAceptar.transform.localPosition.y,
                btnAceptar.transform.localPosition.z);
        }

        if (preguntaActual >= respuestas.Count - 1)
            btnAceptar.transform.GetChild(0).GetComponent<Text>().text = "TERMINAR";
        else btnAceptar.transform.GetChild(0).GetComponent<Text>().text = "SIGUIENTE";
    }

    

	private void colorearRespuesta(){

        //print (iniciaps);
        


        if (respuestasSimple.activeInHierarchy  ) {

            // for (var index = 0; index < txtRespuestasSimple.Count; index++) {
            for (var index = 0; index < txtRespuestasSimple.Count; index++)
            {
                if (respuestas[preguntaActual].GetAnswer() == index) {
                    //txtRespuestasSimple[3].color = answerSettedColor;
                    txtRespuestasSimple[index].color = answerSettedColor;
                    

                }
                else txtRespuestasSimple [index].color = defaultAnswerColor;
                
            }
		
		} else { 
		
			for (var index = 0; index < txtRespuestasMultiple.Count; index++)
				if (respuestas[preguntaActual].GetAnswers().Contains(index))
				{
					txtRespuestasMultiple[index].color = answerSettedColor;
					btnRespuestasMultiple[index].transform.FindChild("paloma").gameObject.SetActive(true);

				}
				else
				{
					txtRespuestasMultiple[index].color = defaultAnswerColor;
					btnRespuestasMultiple[index].transform.FindChild("paloma").gameObject.SetActive(false);
				}
		
		}


	}

	   

    private void mostrarResultados()
    {
        popUps[0].SetActive(false);
        popUps[1].SetActive(true);

        var correctas = 0;
        txtRespuesta.gameObject.SetActive(true);
		txtRespuesta2.gameObject.SetActive (true);
        for (var index = 0; index < respuestas.Count; index++)
        {
            var txtRespuestaClone = Instantiate(txtRespuesta.gameObject);
            txtRespuestaClone.transform.SetParent(txtRespuesta.transform.parent);
            txtRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
            txtRespuestaClone.transform.localPosition = txtRespuesta.transform.localPosition + Vector3.down*index*50;
           

			var txtRespuesta2Clone = Instantiate(txtRespuesta2.gameObject);
			txtRespuesta2Clone.transform.SetParent(txtRespuesta2.transform.parent);
			txtRespuesta2Clone.transform.localScale = new Vector3(1, 1, 1);
			txtRespuesta2Clone.transform.localPosition = txtRespuesta2.transform.localPosition + Vector3.down*index*50;


			var indexpClone = Instantiate(indexp.gameObject);
			indexpClone.transform.SetParent(indexp.transform.parent);
			indexpClone.transform.localScale = new Vector3(1, 1, 1);
			indexpClone.transform.localPosition = indexp.transform.localPosition + Vector3.down*index*50;
			indexpClone.GetComponent<Text>().text = index + 1 + ".";

            if (respuestas[index].ReviewQuestion())
            {
                txtRespuestaClone.GetComponent<Text>().text = " Correcto";
				txtRespuestaClone.GetComponent<Text> ().color = correcta;
				txtRespuesta2Clone.GetComponent<Text> ().text = "2";
                correctas++;
            }
            else
            {
                txtRespuestaClone.GetComponent<Text>().text = " Incorrecto";
				txtRespuestaClone.GetComponent<Text> ().color = incorrecta;
				txtRespuesta2Clone.GetComponent<Text> ().text = "0";
            }
        }
        txtRespuesta.gameObject.SetActive(false);
		txtRespuesta2.gameObject.SetActive (false);
        txtCalificacion.text = "Total" + "   " + correctas*2 + " / 10";
    }

    private void limpiarResultados()
    {
        foreach (Transform child in popUps[1].transform)
            if (child.name == "resultadoPregunta(Clone)") Destroy(child.gameObject);

    }

    // ------------------------Funciones Botones-------------------------

    private void regresar()
    {
        preguntaActual--;
        mostrarPregunta();
    }

    private void avanzar()
    {
        if (preguntaActual >= respuestas.Count - 1)
        {
            StartCoroutine(secuenciaEventos("evaluar", 0.1f));
        }
        else
        {
            preguntaActual++;
            mostrarPregunta();
        }
    }

    private void seleccionarRespuesta(int boton)
    {
        respuestas[preguntaActual].SetAnswer(boton);
        soundScript.playSound("Sound_SeleccionarRespuesta", 1);

		if (respuestas [preguntaActual].GetQuestion ().GetQuestionType () == "single")
			colorearRespuesta ();
		else if (respuestas [preguntaActual].GetQuestion ().GetQuestionType () == "multiplee" || respuestas [preguntaActual].GetQuestion ().GetQuestionType () == "multiple1") {
			colorearRespuesta ();

		}
    }

    // ------------------------Funciones Evaluacion----------------------

    private void seleccionarPreguntasRandom(int numeroPreguntas)
    {
        respuestas = new List<Answer>();
        for (var i = 0; i < numeroPreguntas; i++)
        {
            var randomIndex = Random.Range(0, preguntas.Count);
            respuestas.Add(new Answer(preguntas[randomIndex]));
            preguntas.RemoveAt(randomIndex);
        }
    }

    private void crearCuestionario()
    {
        preguntas = new List<Question>();

        preguntas.Add(new Question("Las leyendas son relatos:\n ",
            new[] { "", "De tradición exclusivamente mexicana",
                    "De tradición oral con elementos reales y ficticios",
                    "Que explican las tragedias de los pueblos"},
            new List<int>(new[] { 2 }),
            "single"
        ));


        preguntas.Add(new Question("En 'La doncella de ojos de esmeralda pasea triste y sola como la profunda noche',\n " +
                                   "la figura retórica es una:",
            new[] { "", "Enumeración", "Metáfora", "Antítesis"},
            new List<int>(new[] { 2 }),
            "single"
        ));


        preguntas.Add(new Question("En 'El inmenso lobo de ojos brillantes aúlla desde entonces todas las noches',\n " +
                                   "los adjetivos calificativos son:",
            new[] { "","Inmenso, brillantes", "Lobo, ojos", "Aúlla, noches" },
            new List<int>(new[] { 1 }),
            "single"
        ));


        preguntas.Add(new Question("En 'La mujer de capa larga, ronda las oscuras noches buscando a su amor perdido',\n " +
                                   "los adjetivos calificativos son:",
            new[] { "","Capa, noches", "Amor, mujer", "Larga, oscuras" },
            new List<int>(new[] { 3 }),
            "single"
        ));


        preguntas.Add(new Question("En 'El sombrero negro se aparece en las noches frías del invierno',\n " +
                                    "los adjetivos calificativos son:",
            new[] { "","Sombrero, noches", "Negro, frías", "Invierno, negro"  },
            new List<int>(new[] { 2 }),
            "single"
        ));


        preguntas.Add(new Question("Selecciona las descripciones que corresponden a la de un personaje ficticio:",
            new[]
            {
                "Hombre de sombrero de copa, con una pipa en la mano.",
                "Lobo de tres ojos que mira las noches estrelladas.",
                "Mujer que flota por las noches con un velo grande y vestido de boda."
            },
            new List<int>(new[] { 1, 2 }),
            "multiplee"
        ));

        preguntas.Add(new Question("Selecciona las descripciones de personajes reales:",
            new[]
            {
                "Niño vestido con short y playera que juega en el bosque.",
                "Joven de capucha negra en la colina las noches de luna llena.",
                "Ave de plumas brillantes y pies azulados."
                
            },
                   new List<int>(new[] { 0, 1 }),
                   "multiplee"
               ));


        preguntas.Add(new Question("Selecciona el compendio al que pertenezca la leyenda 'El lobo triste':",
            new[] { "","Leyendas y lugares", "Leyendas de la fauna local", "Leyendas de la luna"  },
            new List<int>(new[] { 2 }),
            "single"
        ));

        preguntas.Add(new Question("Selecciona el compendio al que pertenezca la leyenda 'La mujer de la Revolución':",
            new[] { "","Leyendas de Sonora", "Leyendas de terror", "Leyendas del campo" },
            new List<int>(new[] { 1 }),
            "single"
        ));


        preguntas.Add(new Question("Selecciona el compendio al que pertenezca la leyenda 'La sirena vaga':",
            new[] { "","Leyendas de mujeres temerosas", "Leyendas de mujeres históricas", "Leyendas del Pacífico" },
            new List<int>(new[] { 3 }),
            "single"
        ));

        preguntas.Add(new Question("Selecciona la descripción que corresponde a la de un personaje ficticio:",
            new[] { "","Perro de ojos verdes que no se separa de su dueño",
                    "Pájaro de grandes alas y colores vivos",
                    "León lanza fuego de dientes filosos" },
            new List<int>(new[] { 3 }),
            "single"
        ));

        preguntas.Add(new Question("Selecciona la descripción que corresponde a un personaje real:",
            new[] { "","León marino de gran tamaño",
                    "Niño alado",
                    "Gato de dientes filosos y finas alas" },
            new List<int>(new[] { 1 }),
            "single"
        ));
           
                
        
        
    }
}

public class Question
{
    private readonly string[] _answersTexts;
    private readonly List<int> _correctAnswers;
    private readonly string _questionText;
    private readonly string _questionType;

    /// <summary>
    ///     Question with only one correct answer
    /// </summary>
    /// <param name="questionTest">Here comes the question string</param>
    /// <param name="answerTexts">Here comes all the text of each answer</param>
    /// <param name="correctAnswer">Correct Answer, NOTE it has to be int starting in 0</param>
    public Question(string questionTest, string[] answerTexts, List<int> correctAnswers, string questionType)
    {
        _questionText = questionTest;
        _answersTexts = answerTexts;
        _correctAnswers = correctAnswers;
        _questionType = questionType;
    }


    public string GetQuestionText()
    {
        return _questionText;
    }

    public string[] GetAnserTexts()
    {
        return _answersTexts;
    }

    public string GetQuestionType()
    {
        return _questionType;
    }

    public List<int> GetCorrectAnswers()
    {
        return _correctAnswers;
    }
}

public class Answer
{
    private List<int> _answers;
    private readonly Question _question;
    private int _answer;


    /// <summary>
    ///     Answer for a single answer question
    /// </summary>
    /// <param name="question">Question object</param>
    /// <param name="answer">Answer to the question object</param>
    public Answer(Question question)
    {
        _question = question;
        _answers = new List<int>();

    }

    public Answer(Question question, int answer)
    {
        _question = question;
        _answer = answer;
    }



    public Question GetQuestion()
    {
        return _question;
    }

    public List<int> GetAnswers()
    {
        return _answers;
    }

    public void SetAnswers(List<int> answers)
    {
        _answers = answers;
    }

    //Set and unSet answer
    public void SetAnswer(int answer)
    {

      
        if (_answers.Contains(answer))
        {
            _answers.Remove(answer);

        }
        else
        {
            _answers.Add(answer);
           
        }

        _answer = answer;

    }


    public int GetAnswer()
    {
        return _answer;
    }

  



    //Checa que las respuestas son correctas
    public bool ReviewQuestion()
    {
        var correctAnswers = _question.GetCorrectAnswers();
        foreach (var correctAnswer in correctAnswers)
            if (!_answers.Contains(correctAnswer))
                return false;

        foreach (var answer in _answers)
            if (!correctAnswers.Contains(answer))
                return false;

        return true;
    }
}






