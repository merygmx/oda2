﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Reto2 : MonoBehaviour
{
    private int banderaopcion;
    private int rand2;
    private int misionactual = 1;
    
    private Button btnmochila;
    private Button btnaceptarr2;
    private Button btnpierdevidas;
    private Button btnaceptartimeout;
    private Button btnaceptarnoacerto;
    private Button btnaceptarexito;
    private Button btnreto3;
    private MyTimer cronometro;
    private bool mis1 = false;
    private bool mis2 = false;
    private bool mis3 = false;
    private bool flag;
    private bool dragging;
    private bool dragging2;
    private bool m = false;
    private bool broca;
    private bool barena;
    private bool bpuerta;
    private GameObject mochilam1;
    private GameObject llavem1;
    private GameObject palam1;
    private GameObject picom1;
    private GameObject rocam1;
    private GameObject posrocam1;
    private GameObject rocaoffm1;
    private GameObject mochilam2;
    private GameObject llavem2;
    private GameObject palam2;
    private GameObject picom2;
    private GameObject rocam2;
    private GameObject arenam2;
    private GameObject posarenam2;
    private GameObject arenaoffm2;
    private GameObject mochilam3;
    private GameObject llavem3;
    private GameObject palam3;
    private GameObject picom3;
    private GameObject puertam3;
    private GameObject puertaoffm3;
    private GameObject pospuertam3;
    private GameObject Mision1;
    private GameObject Mision2;
    private GameObject Mision3;
    private GameObject Reto2Obj;
    private GameObject Reto3;
    private GameObject Mision1Opcion2;
    private GameObject manecillaa;
    private GameObject popuppierdevidas;
    private GameObject popupexito;
    private GameObject popupnoacerto;
    private GameObject popuptimeout;
    private GameObject brillo;
    private GameObject corazongana;
    //private GameObject corazonretos;
    //private GameObject corazonextra;
    private GameObject corazon;
	private GameObject corazonm1;
	private GameObject corazonm2;
	private GameObject corazonm3;
    private GameObject Mision1Opcion3;
    private GameObject Mision2Opcion1;
    private GameObject Mision2Opcion2;
    private GameObject Mision2Opcion3;
    private GameObject Mision3Opcion1;
    private GameObject Mision3Opcion2;
    private GameObject Mision3Opcion3;
    private GameObject Pantalla5;
    private GameObject popupr2;
    private GameObject Pantalla5b;
    private GameObject elem;
    private GameObject elem2;
    private GameObject puerta;
    private GameObject roca;
    private GameObject arena;
    private GameObject mochila;
    private GameObject llave;
    private GameObject pala;
    private GameObject pico;
    private GameObject rocaoff;
    private GameObject arenaoff;
    private GameObject puertaoff;
    private GameObject llaveoff;
    private GameObject palaoff;
    private GameObject picoff;
    private GameObject posarena;
    private GameObject posroca;
    private GameObject pospuerta;
    private GameObject Pantalla6;
    private GameObject inerleyenda;
    private GameObject zona2;
    private GameObject reloj;
    private GameObject Mision1Opcion1;
    private GameObject frase;
    private GameObject imagen1;
    private GameObject imagen2;
    private GameObject imagen3;
    private GameObject imagen3g;
    private GameObject imagen2g;
    private GameObject imagen1g;
    private GameObject imagen1m2;
    private GameObject imagen2m2;
    private GameObject imagen3m2;
    private GameObject imagen3gm2;
    private GameObject imagen2gm2;
    private GameObject imagen1gm2;
    private GameObject imagen1m3;
    private GameObject imagen2m3;
    private GameObject imagen3m3;
    private GameObject imagen3gm3;
    private GameObject imagen2gm3;
    private GameObject imagen1gm3;
    private GameObject imagen1Opcion1m1;
    private GameObject imagen1Opcion2m1;
    private GameObject imagen1Opcion3m1;
    private GameObject imagen1Opcion1m2;
    private GameObject imagen1Opcion2m2;
    private GameObject imagen1Opcion3m2;
    private GameObject imagen1Opcion1m3;
    private GameObject imagen1Opcion2m3;
    private GameObject imagen1Opcion3m3;
    private GameObject imagen2Opcion1m1;
    private GameObject imagen2Opcion2m1;
    private GameObject imagen2Opcion3m1;
    private GameObject imagen2Opcion1m2;
    private GameObject imagen2Opcion2m2;
    private GameObject imagen2Opcion3m2;
    private GameObject imagen2Opcion1m3;
    private GameObject imagen2Opcion2m3;
    private GameObject imagen2Opcion3m3;
    private GameObject imagen3Opcion1m1;
    private GameObject imagen3Opcion2m1;
    private GameObject imagen3Opcion3m1;
    private GameObject imagen3Opcion1m2;
    private GameObject imagen3Opcion2m2;
    private GameObject imagen3Opcion3m2;
    private GameObject imagen3Opcion1m3;
    private GameObject imagen3Opcion2m3;
    private GameObject imagen3Opcion3m3;

    private Vector3 posIni;
    private Vector3 posIni2;

    private SoundManager soundManager;
    // Use this for initialization
    private void Start()
    {
        Reto3 = transform.FindChild("Reto3").gameObject;


        Reto2Obj = transform.FindChild("Reto2").gameObject;
        Reto2Obj.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
        Pantalla5 = Reto2Obj.transform.GetChild(0).FindChild("Pantalla5").gameObject;
        popupr2 = Pantalla5.transform.FindChild("popupr2").gameObject;
        btnaceptarr2 = popupr2.transform.FindChild("btnaceptar").GetComponent<Button>();
        btnmochila = Pantalla5.transform.FindChild("btnmochila").GetComponent<Button>();

        Mision1 = Reto2Obj.transform.GetChild(0).FindChild("Mision1").gameObject;
        mochilam1 = Mision1.transform.FindChild("mochila").gameObject;
        llavem1 = Mision1.transform.FindChild("llave").gameObject;
        palam1 = Mision1.transform.FindChild("pala").gameObject;
        picom1 = Mision1.transform.FindChild("pico").gameObject;
        rocam1 = Mision1.transform.FindChild("roca").gameObject;
        posrocam1 = Mision1.transform.FindChild("posroca").gameObject;
        rocaoffm1 = Mision1.transform.FindChild("rocaoff").gameObject;
		corazonm1 = Mision1.transform.FindChild("corazonr2").gameObject;

        Mision2 = Reto2Obj.transform.GetChild(0).FindChild("Mision2").gameObject;
        mochilam2 = Mision2.transform.FindChild("mochila").gameObject;
        llavem2 = Mision2.transform.FindChild("llave").gameObject;
        palam2 = Mision2.transform.FindChild("pala").gameObject;
        picom2 = Mision2.transform.FindChild("picoff").gameObject;
        //rocam2 = Mision2.transform.FindChild ("roca").gameObject;
        arenam2 = Mision2.transform.FindChild("arena").gameObject;
        posarenam2 = Mision2.transform.FindChild("posarena").gameObject;
        arenaoffm2 = Mision2.transform.FindChild("arenaoff").gameObject;
		corazonm2 = Mision2.transform.FindChild("corazonr2").gameObject;

        Mision3 = Reto2Obj.transform.GetChild(0).FindChild("Mision3").gameObject;
        mochilam3 = Mision3.transform.FindChild("mochila").gameObject;
        llavem3 = Mision3.transform.FindChild("llave").gameObject;
        palam3 = Mision3.transform.FindChild("palaoff").gameObject;
        picom3 = Mision3.transform.FindChild("picoff").gameObject;
        puertam3 = Mision3.transform.FindChild("puerta").gameObject;
        puertaoffm3 = Mision3.transform.FindChild("puertaoff").gameObject;
        pospuertam3 = Mision3.transform.FindChild("pospuerta").gameObject;
		corazonm3 = Mision3.transform.FindChild("corazonr2").gameObject;


        soundManager = transform.GetComponent<SoundManager>();
        cronometro = transform.GetComponent<MyTimer>();
        soundManager.playSound("Fondo_lobo3", 0);


        Pantalla6 = Reto2Obj.transform.GetChild(0).FindChild("Pantalla6").gameObject;
        inerleyenda = Pantalla6.transform.FindChild("inerleyenda").gameObject;
        //zona2 = inerleyenda.transform.FindChild ("zona2").gameObject;
        reloj = inerleyenda.transform.FindChild("reloj").gameObject;
		manecillaa = inerleyenda.transform.FindChild ("manecillaa").gameObject;
        //manecillaa = inerleyenda.transform.FindChild("manecillaa").gameObject;
        //PlayerPrefs.SetInt("corazonesODA2_ODA2", 1);
        corazon = Pantalla6.transform.FindChild("corazonr2").gameObject;
        popuppierdevidas = Pantalla6.transform.FindChild("popuppierdevidas").gameObject;
        btnpierdevidas = popuppierdevidas.transform.FindChild("btnaceptar").GetComponent<Button>();
        popupexito = Pantalla6.transform.FindChild("popupexito").gameObject;
        btnaceptarexito = popupexito.transform.FindChild("btnaceptar").GetComponent<Button>();
        popuptimeout = Pantalla6.transform.FindChild("popuptimeout").gameObject;
        btnaceptartimeout = popuptimeout.transform.FindChild("btnaceptar").GetComponent<Button>();
        popupnoacerto = Pantalla6.transform.FindChild("popupnoacerto").gameObject;
        btnaceptarnoacerto = popupnoacerto.transform.FindChild("btnvolverintentar").GetComponent<Button>();
        btnreto3 = popupnoacerto.transform.FindChild("btnreto3").GetComponent<Button>();

        brillo = Pantalla6.transform.FindChild("brillo").gameObject;
        corazongana = Pantalla6.transform.FindChild("corazongana").gameObject;
        //corazonextra = Pantalla6.transform.FindChild("corazonextra").gameObject;
        //corazonretos = Pantalla6.transform.FindChild("corazonretos").gameObject;


        //mision1
        Mision1Opcion1 = inerleyenda.transform.FindChild("Mision1Opcion1").gameObject;
        frase = Mision1Opcion1.transform.FindChild("frase").gameObject;

        //imagen1= Mision2Opcion1.transform.FindChild (randomOption).gameObject;
        imagen1 = Mision1Opcion1.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision1Opcion1.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision1Opcion1.transform.FindChild("imagen3").gameObject;
        imagen3Opcion1m1 = Mision1Opcion1.transform.FindChild("imagen3Opcion1m1").gameObject;
        imagen2Opcion1m1 = Mision1Opcion1.transform.FindChild("imagen2Opcion1m1").gameObject;
        imagen1Opcion1m1 = Mision1Opcion1.transform.FindChild("imagen1Opcion1m1").gameObject;
        zona2 = inerleyenda.transform.FindChild("zona22").gameObject;

        Mision1Opcion2 = inerleyenda.transform.FindChild("Mision1Opcion2").gameObject;
        frase = Mision1Opcion2.transform.FindChild("frase").gameObject;
        imagen1 = Mision1Opcion2.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision1Opcion2.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision1Opcion2.transform.FindChild("imagen3").gameObject;
        imagen3Opcion2m1 = Mision1Opcion2.transform.FindChild("imagen3Opcion2m1").gameObject;
        imagen2Opcion2m1 = Mision1Opcion2.transform.FindChild("imagen2Opcion2m1").gameObject;
        imagen1Opcion2m1 = Mision1Opcion2.transform.FindChild("imagen1Opcion2m1").gameObject;


        Mision1Opcion3 = inerleyenda.transform.FindChild("Mision1Opcion3").gameObject;
        frase = Mision1Opcion3.transform.FindChild("frase").gameObject;
        imagen1 = Mision1Opcion3.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision1Opcion3.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision1Opcion3.transform.FindChild("imagen3").gameObject;
        imagen3Opcion3m1 = Mision1Opcion3.transform.FindChild("imagen3Opcion3m1").gameObject;
        imagen2Opcion3m1 = Mision1Opcion3.transform.FindChild("imagen2Opcion3m1").gameObject;
        imagen1Opcion3m1 = Mision1Opcion3.transform.FindChild("imagen1Opcion3m1").gameObject;


        //mision2
        Mision2Opcion1 = inerleyenda.transform.FindChild("Mision2Opcion1").gameObject;
        frase = Mision2Opcion1.transform.FindChild("frase").gameObject;

        //string randomOption = "imagen1_" + "opcion" + Random.Range (1, 4) + "_m" + misionActual;

        //imagen1= Mision2Opcion1.transform.FindChild (randomOption).gameObject;

        imagen1 = Mision2Opcion1.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision2Opcion1.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision2Opcion1.transform.FindChild("imagen3").gameObject;
        imagen3Opcion1m2 = Mision2Opcion1.transform.FindChild("imagen3Opcion1m2").gameObject;
        imagen2Opcion1m2 = Mision2Opcion1.transform.FindChild("imagen2Opcion1m2").gameObject;
        imagen1Opcion1m2 = Mision2Opcion1.transform.FindChild("imagen1Opcion1m2").gameObject;

        Mision2Opcion2 = inerleyenda.transform.FindChild("Mision2Opcion2").gameObject;
        frase = Mision2Opcion2.transform.FindChild("frase").gameObject;
        imagen1 = Mision2Opcion2.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision2Opcion2.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision2Opcion2.transform.FindChild("imagen3").gameObject;
        imagen3Opcion2m2 = Mision2Opcion2.transform.FindChild("imagen3Opcion2m2").gameObject;
        imagen2Opcion2m2 = Mision2Opcion2.transform.FindChild("imagen2Opcion2m2").gameObject;
        imagen1Opcion2m2 = Mision2Opcion2.transform.FindChild("imagen1Opcion2m2").gameObject;


        Mision2Opcion3 = inerleyenda.transform.FindChild("Mision2Opcion3").gameObject;
        frase = Mision2Opcion3.transform.FindChild("frase").gameObject;
        imagen1 = Mision2Opcion3.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision2Opcion3.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision2Opcion3.transform.FindChild("imagen3").gameObject;
        imagen3Opcion3m2 = Mision2Opcion3.transform.FindChild("imagen3Opcion3m2").gameObject;
        imagen2Opcion3m2 = Mision2Opcion3.transform.FindChild("imagen2Opcion3m2").gameObject;
        imagen1Opcion3m2 = Mision2Opcion3.transform.FindChild("imagen1Opcion3m2").gameObject;


        //mision3
        Mision3Opcion1 = inerleyenda.transform.FindChild("Mision3Opcion1").gameObject;
        frase = Mision3Opcion1.transform.FindChild("frase").gameObject;
        imagen1 = Mision3Opcion1.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision3Opcion1.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision3Opcion1.transform.FindChild("imagen3").gameObject;
        imagen3Opcion1m3 = Mision3Opcion1.transform.FindChild("imagen3Opcion1m3").gameObject;
        imagen2Opcion1m3 = Mision3Opcion1.transform.FindChild("imagen2Opcion1m3").gameObject;
        imagen1Opcion1m3 = Mision3Opcion1.transform.FindChild("imagen1Opcion1m3").gameObject;

        Mision3Opcion2 = inerleyenda.transform.FindChild("Mision3Opcion2").gameObject;
        frase = Mision3Opcion2.transform.FindChild("frase").gameObject;
        imagen1 = Mision3Opcion2.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision3Opcion2.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision3Opcion2.transform.FindChild("imagen3").gameObject;
        imagen3Opcion2m3 = Mision3Opcion2.transform.FindChild("imagen3Opcion2m3").gameObject;
        imagen2Opcion2m3 = Mision3Opcion2.transform.FindChild("imagen2Opcion2m3").gameObject;
        imagen1Opcion2m3 = Mision3Opcion2.transform.FindChild("imagen1Opcion2m3").gameObject;


        Mision3Opcion3 = inerleyenda.transform.FindChild("Mision3Opcion3").gameObject;
        frase = Mision3Opcion3.transform.FindChild("frase").gameObject;
        imagen1 = Mision3Opcion3.transform.FindChild("imagen1").gameObject;
        imagen2 = Mision3Opcion3.transform.FindChild("imagen2").gameObject;
        imagen3 = Mision3Opcion3.transform.FindChild("imagen3").gameObject;
        imagen3Opcion3m3 = Mision3Opcion3.transform.FindChild("imagen3Opcion3m3").gameObject;
        imagen2Opcion3m3 = Mision3Opcion3.transform.FindChild("imagen2Opcion3m3").gameObject;
        imagen1Opcion3m3 = Mision3Opcion3.transform.FindChild("imagen1Opcion3m3").gameObject;


        btnmochila.onClick.AddListener(delegate
        {
            soundManager.playSound("Abrir_Mochila", 1);
				Invoke("iniciar", 0.5f);
        });


        btnaceptartimeout.onClick.AddListener(delegate
        {
            cronometro.StopTimer();
           // popuptimeout.SetActive(false);
            PlayerPrefs.SetInt("corazonestest", 0);
            popuptimeout.SetActive(false);
            soundManager.playSound("Sound_PerderVida10", 1);

            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
                PlayerPrefs.GetInt("corazonestest").ToString();

            //reiniciar reto
            irmision1();
            
				

            
        });


        btnaceptarr2.onClick.AddListener(delegate
        {
            popupr2.SetActive(false);
            Pantalla5.SetActive(false);
            //Pantalla5b.SetActive(true);
            Mision1Opcion1.SetActive(false);
            Mision1Opcion2.SetActive(false);
            Mision1Opcion3.SetActive(false);

            Mision2Opcion1.SetActive(false);
            Mision2Opcion2.SetActive(false);
            Mision2Opcion3.SetActive(false);

            Mision3Opcion1.SetActive(false);
            Mision3Opcion2.SetActive(false);
            Mision3Opcion3.SetActive(false);
            irmision1();
            
        });

        btnpierdevidas.onClick.AddListener(delegate
        {
            cronometro.StopTimer();
            popuppierdevidas.SetActive(false);
            
            
				PlayerPrefs.SetInt("corazonestest",0);
            
				corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
            
            irmision1();
        });

        btnaceptarexito.onClick.AddListener(delegate
        {

			popupexito.SetActive(false);
            Pantalla5.SetActive(false);
            Pantalla6.SetActive(false);
            Mision1.SetActive(false);
            Mision2.SetActive(false);
            Mision3.SetActive(false);
			//corazonesreto=4;
						
			Reto3.SetActive(true);
            soundManager.playSound("Sound_SonidoFondo2", 0);
            //numcorazones = 1;
        });


        btnaceptarnoacerto.onClick.AddListener(delegate
        {
            cronometro.StopTimer();
            Pantalla6.SetActive(false);
            Pantalla5.SetActive(false);
            popupnoacerto.SetActive(false);
            Mision1.SetActive(false);
            Mision2.SetActive(false);
            Mision3.SetActive(false);
            irmision1();
        });


        btnreto3.onClick.AddListener(delegate
        {
				
			popupnoacerto.SetActive(false);
            Pantalla6.SetActive(false);
            Pantalla5.SetActive(false);
            popupnoacerto.SetActive(false);
            Mision1.SetActive(false);
            Mision2.SetActive(false);
            Mision3.SetActive(false);
            Reto3.SetActive(true);
            soundManager.playSound("Sound_SonidoFondo2", 0);
			
        });
		//cronometro.StartTimer (1);
    }

    // Update is called once per frame
    private void Update()
    {
        
		manecillaa.transform.eulerAngles = new Vector3 (0, 0, -1 * cronometro.remainingTime * 6);




        if (Input.touchCount > 0)
        {
            var pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            var hit = Physics2D.Raycast(pos, Vector2.zero);

            var pos2 = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            var hit2 = Physics2D.Raycast(pos2, Vector2.zero);

            if ((hit.collider != null) && (Input.GetTouch(0).phase == TouchPhase.Began))
            {
                //				posIni = pos;
                //print ("Startedss");
                Debug.Log(hit.collider.gameObject.name);
                try
                {
                    print(hit.collider.gameObject.name);
                    //pos = hit.collider.gameObject.transform.localPosition;
                    if ((hit.collider.gameObject.name == "llave") || (hit.collider.name == "pico") ||
                        (hit.collider.gameObject.name == "pala"))
                    {
                        elem = hit.collider.gameObject;
                        //print ("agarra elemento");	
                        posIni = hit.collider.gameObject.transform.localPosition;
                        dragging = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log(ex.Message);
                }
            }

            //touch de inerleyendas
            if ((hit2.collider != null) && (Input.GetTouch(0).phase == TouchPhase.Began))
            {
                //				posIni2 = pos2;
                //print("Startedss");
                Debug.Log(hit2.collider.gameObject.name);
                try
                {
                    print(hit2.collider.gameObject.name);
                    //pos = hit.collider.gameObject.transform.localPosition;
                    if ((hit2.collider.gameObject.name == "imagen1") || (hit2.collider.name == "imagen2") ||
                        (hit2.collider.gameObject.name == "imagen3"))
                    {
                        elem2 = hit2.collider.gameObject;
                        //print("agarra elemento");
                        posIni2 = hit2.collider.gameObject.transform.localPosition;
                        dragging2 = true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log(ex.Message);
                }
            }


            if (Input.GetTouch(0).phase == TouchPhase.Moved)
                if (dragging)
                {
                    //print ("Testing" + elem.name);
                    elem.transform.position = new Vector3(pos.x, pos.y + .5f, elem.transform.position.z);
                }
                else
                {
                    //touch de inerleyendas
                    if (dragging2)
                        elem2.transform.position = new Vector3(pos2.x, pos2.y + .5f, elem2.transform.position.z);
                }


            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (dragging)
                {
                    if ((Vector3.Distance(elem.transform.localPosition, posrocam1.transform.localPosition) < 150) &&
                        (elem.name == "pico"))
                    {
                        elem.transform.localPosition = posrocam1.transform.localPosition;
                        broca = true;


                        soundManager.playSound("Copia_de_Sound_Pico", 1);
                        //rocaoff.SetActive (true);
                        rocaoffm1.SetActive(false);
                        misionactual = 1;
                        selmisiones();
                        picom1.SetActive(false);
                        elem.transform.localPosition = posIni;
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, posarenam2.transform.localPosition) < 150) &&
                             (elem.name == "pala"))
                    {
                        elem.transform.localPosition = posarenam2.transform.localPosition;
                        barena = true;
                        arenaoffm2.SetActive(false);
                        //arenaoff.SetActive (true);
                        misionactual = 2;
                        selmisiones();
                        palam2.SetActive(false);
                        elem.transform.localPosition = posIni;
                    }
                    else if ((Vector3.Distance(elem.transform.localPosition, pospuertam3.transform.localPosition) < 150) &&
                             (elem.name == "llave"))
                    {
                        elem.transform.localPosition = pospuertam3.transform.localPosition;
                        bpuerta = true;
                        soundManager.playSound("Sound_Abrir", 1);
                        puertaoffm3.SetActive(false);
                        misionactual = 3;
                        selmisiones();
                        llavem3.SetActive(false);
                        elem.transform.localPosition = posIni;
                    }
                    else
                    {
                        elem.transform.localPosition = posIni;
                    }

                    dragging = false;
                } //fin de draggin


                if (dragging2)
                {
                    //prender popup timeout
					int aux=1;

                    //activar cronómetro
                    //cronometro.StartTimer(30);

                    if ((Vector3.Distance(elem2.transform.localPosition, zona2.transform.localPosition) < 300) &&
                        (elem2.name == "imagen1"))
                    {
                        elem2.transform.localPosition = zona2.transform.localPosition;
                        //Invoke("revisarcronometro", 0f);
                        //desactivar imagen3 
                        imagen3Opcion1m1.SetActive(false);
                        imagen3Opcion2m1.SetActive(false);
                        imagen3Opcion3m1.SetActive(false);

                        imagen3Opcion1m2.SetActive(false);
                        imagen3Opcion2m2.SetActive(false);
                        imagen3Opcion3m2.SetActive(false);

                        imagen3Opcion1m3.SetActive(false);
                        imagen3Opcion2m3.SetActive(false);
                        imagen3Opcion3m3.SetActive(false);

                        //desactivar imagen2
                        imagen2Opcion1m1.SetActive(false);
                        imagen2Opcion2m1.SetActive(false);
                        imagen2Opcion3m1.SetActive(false);

                        imagen2Opcion1m2.SetActive(false);
                        imagen2Opcion2m2.SetActive(false);
                        imagen2Opcion3m2.SetActive(false);

                        imagen2Opcion1m3.SetActive(false);
                        imagen2Opcion2m3.SetActive(false);
                        imagen2Opcion3m3.SetActive(false);

                        //mision1 opcion1
                        //mision1 opcion2
                        //mision1 opcion3


                        if ((misionactual == 1) && (banderaopcion == 1))
                        {
                            imagen1Opcion1m1.SetActive(true);
                           

                            //ganar corazon
                            //numcorazones++;
                            //corazonesreto = 1;
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
                            soundManager.playSound("Sound_correcto10", 1);
                            corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
							//corazonretos.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = corazonesreto.ToString();
                            elem2.transform.localPosition = posIni2;
                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            //Invokar mision2
                            Invoke("irmision2", 1f);
                            //detener cronometro
                            
                        }

                        if ((misionactual == 1) && (banderaopcion == 2))
                        {
                            imagen1Opcion2m1.SetActive(true);
                            

                            aux =PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 1) && (banderaopcion == 3))
                        {
                            imagen1Opcion3m1.SetActive(true);
                          
                            //ganar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
							elem2.transform.localPosition = posIni2;
                            Invoke("revisarcronometro", 0f);
                            //detener cronometro
                            cronometro.StopTimer();
                            //Invokar mision2
                            Invoke("irmision2", 1f);
                            
                        }


                        //mision2 opcion1
                        //mision2 opcion 2
                        //mision2 opcion 3
                        if ((misionactual == 2) && (banderaopcion == 1))
                        {
                            imagen1Opcion1m2.SetActive(true);
                            
                            //quitar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 2) && (banderaopcion == 2))
                        {
                            imagen1Opcion2m2.SetActive(true);
                            
                            //quitar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 2) && (banderaopcion == 3))
                        {
                            imagen1Opcion3m2.SetActive(true);
                           
                            //ganar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            //Invokar mision3
                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            Invoke("irmision3", 1f);
                            //detener cronometro
                            
                        }


                        if ((misionactual == 3) && (banderaopcion == 1))
                        {
                            imagen1Opcion1m3.SetActive(true);
                          
                            //quitar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                           
                            Invoke("revisarcronometro", 0f);
                            Invoke("revisarpuntuacion", 10f);
                        }

                        if ((misionactual == 3) && (banderaopcion == 2))
                        {
                            imagen1Opcion2m3.SetActive(true);
                           
                            //ganar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;

                           
                            //detener cronometro
                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            //Invokar revision de puntuación
                            Invoke("revisarpuntuacion", 10f);

                        }

                        if ((misionactual == 3) && (banderaopcion == 3))
                        {
                            imagen1Opcion3m3.SetActive(true);
                            
                            //ganar corazon
                            aux =PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
							//corazonretos.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = corazonesreto.ToString();

                            elem2.transform.localPosition = posIni2;

                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            //Invokar revision de puntuación
                            Invoke("revisarpuntuacion", 10f);
                        }
                    }
                    else if ((Vector3.Distance(elem2.transform.localPosition, zona2.transform.localPosition) < 300) &&
                             (elem2.name == "imagen2"))
                    {
                        elem2.transform.localPosition = zona2.transform.localPosition;
                       // Invoke("revisarcronometro", 0f);
                        //desactivar imagen1 
                        imagen1Opcion1m1.SetActive(false);
                        imagen1Opcion2m1.SetActive(false);
                        imagen1Opcion3m1.SetActive(false);

                        imagen1Opcion1m2.SetActive(false);
                        imagen1Opcion2m2.SetActive(false);
                        imagen1Opcion3m2.SetActive(false);

                        imagen1Opcion1m3.SetActive(false);
                        imagen1Opcion2m3.SetActive(false);
                        imagen1Opcion3m3.SetActive(false);

                        //desactivar imagen3
                        imagen3Opcion1m1.SetActive(false);
                        imagen3Opcion2m1.SetActive(false);
                        imagen3Opcion3m1.SetActive(false);

                        imagen3Opcion1m2.SetActive(false);
                        imagen3Opcion2m2.SetActive(false);
                        imagen3Opcion3m2.SetActive(false);

                        imagen3Opcion1m3.SetActive(false);
                        imagen3Opcion2m3.SetActive(false);
                        imagen3Opcion3m3.SetActive(false);

                        //mision1 opcion1
                        //mision1 opcion2
                        //mision1 opcion3


                        if ((misionactual == 1) && (banderaopcion == 1))
                        {
                            imagen2Opcion1m1.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 1) && (banderaopcion == 2))
                        {
                            imagen2Opcion2m1.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 1) && (banderaopcion == 3))
                        {
                            imagen2Opcion3m1.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }


                        //mision2 opcion1
                        //mision2 opcion 2
                        //mision2 opcion 3
                        if ((misionactual == 2) && (banderaopcion == 1))
                        {
                            imagen2Opcion1m2.SetActive(true);
                            //ganar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;

                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            //Invokar revision de puntuación
                            Invoke("revisarpuntuacion", 1f);

                            //Invokar mision3
                            Invoke("irmision3", 1f);
                           
                        }

                        if ((misionactual == 2) && (banderaopcion == 2))
                        {
                            imagen2Opcion2m2.SetActive(true);
                            //ganar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
							//corazonretos.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = corazonesreto.ToString();

                            elem2.transform.localPosition = posIni2;

                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            //Invokar revision de puntuación
                            Invoke("revisarpuntuacion", 1f);

                            //Invokar mision3
                            Invoke("irmision3", 1f);
                        }


                        if ((misionactual == 2) && (banderaopcion == 3))
                        {
                            imagen2Opcion3m2.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        //mision3 opcion1
                        //mision3 opcion 2
                        //mision3 opcion 3
                        if ((misionactual == 3) && (banderaopcion == 1))
                        {
                            imagen2Opcion1m3.SetActive(true);
                            //ganar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
							//corazonretos.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = corazonesreto.ToString();

                            elem2.transform.localPosition = posIni2;
                            //detener cronometro
                            cronometro.StopTimer();
                            //Invokar revision de puntuación
                            //Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarpuntuacion", 10f);

                        }

                        if ((misionactual == 3) && (banderaopcion == 2))
                        {
                            imagen2Opcion2m3.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            
                            Invoke("revisarcronometro", 0f);
                            Invoke("revisarpuntuacion", 10f);
                        }

                        if ((misionactual == 3) && (banderaopcion == 3))
                        {
                            imagen2Opcion3m3.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            
                            Invoke("revisarcronometro", 0f);
                            Invoke("revisarpuntuacion", 10f);
                        }
                    }
                    else if ((Vector3.Distance(elem2.transform.localPosition, zona2.transform.localPosition) < 300) &&
                             (elem2.name == "imagen3"))
                    {
                        elem2.transform.localPosition = zona2.transform.localPosition;

                       // Invoke("revisarcronometro", 0f);
                        //desactivar imagen1 
                        imagen1Opcion1m1.SetActive(false);
                        imagen1Opcion2m1.SetActive(false);
                        imagen1Opcion3m1.SetActive(false);

                        imagen1Opcion1m2.SetActive(false);
                        imagen1Opcion2m2.SetActive(false);
                        imagen1Opcion3m2.SetActive(false);

                        imagen1Opcion1m3.SetActive(false);
                        imagen1Opcion2m3.SetActive(false);
                        imagen1Opcion3m3.SetActive(false);

                        //desactivar imagen2
                        imagen2Opcion1m1.SetActive(false);
                        imagen2Opcion2m1.SetActive(false);
                        imagen2Opcion3m1.SetActive(false);

                        imagen2Opcion1m2.SetActive(false);
                        imagen2Opcion2m2.SetActive(false);
                        imagen2Opcion3m2.SetActive(false);

                        imagen2Opcion1m3.SetActive(false);
                        imagen2Opcion2m3.SetActive(false);
                        imagen2Opcion3m3.SetActive(false);


                        //mision1 opcion1
                        //mision1 opcion2
                        //mision1 opcion3

                        if ((misionactual == 1) && (banderaopcion == 1))
                        {
                            imagen3Opcion1m1.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 1) && (banderaopcion == 2))
                        {
                            imagen3Opcion2m1.SetActive(true);
                            //ganar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux++;
							print ("Auxiliar : " + aux);
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_correcto10", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
							//corazonretos.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = corazonesreto.ToString();

                            elem2.transform.localPosition = posIni2;

                            Invoke("revisarcronometro", 0f);
                            cronometro.StopTimer();
                            //Invokar revision de puntuación
                            Invoke("revisarpuntuacion", 1f);

                            //Invokar mision2
                            Invoke("irmision2", 1f);
                            
                        }

                        if ((misionactual == 1) && (banderaopcion == 3))
                        {
                            imagen3Opcion3m1.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }


                        //mision2 opcion1
                        //mision2 opcion 2
                        //mision2 opcion 3
                        if ((misionactual == 2) && (banderaopcion == 1))
                        {
                            imagen3Opcion1m2.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 2) && (banderaopcion == 2))
                        {
                            imagen3Opcion2m2.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }

                        if ((misionactual == 2) && (banderaopcion == 3))
                        {
                            imagen3Opcion3m2.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            Invoke("revisarpuntuacion", 1f);
                            Invoke("revisarcronometro", 0f);
                        }


                        //mision3 opcion1
                        //mision3 opcion 2
                        //mision3 opcion 3
                        if ((misionactual == 3) && (banderaopcion == 1))
                        {
                            imagen3Opcion1m3.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            
                            Invoke("revisarcronometro", 0f);
                            Invoke("revisarpuntuacion", 10f);
                        }

                        if ((misionactual == 3) && (banderaopcion == 2))
                        {
                            imagen3Opcion2m3.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            
                            Invoke("revisarcronometro", 0f);
                            Invoke("revisarpuntuacion", 10f);
                        }

                        if ((misionactual == 3) && (banderaopcion == 3))
                        {
                            imagen3Opcion3m3.SetActive(true);
                            //quitar corazon
							aux=PlayerPrefs.GetInt("corazonestest");
							aux--;
							PlayerPrefs.SetInt("corazonestest",aux);
							soundManager.playSound("Sound_incorrecto19", 1);
							corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
								PlayerPrefs.GetInt("corazonestest").ToString();
                            elem2.transform.localPosition = posIni2;
                            //revisar puntuación
                            
                            Invoke("revisarcronometro", 0f);
                            Invoke("revisarpuntuacion", 10f);
                        }
                    }
                    else
                    {
                        elem2.transform.localPosition = posIni2;
                        //popUpErr.SetActive (true);
                    }


					if ((PlayerPrefs.GetInt("corazonestest") >= 3) && (misionactual == 3))
                    {
                        //se gana una vida extra y sale el corazon en medio de la pantalla con brillo 
                        //se agrega en la parte superior derecha de la pantalla y sale popup)
                       
                        //agregar en la parte sup. derecha de la pantalla
                        inerleyenda.SetActive(false);
                        brillo.SetActive(true);
                        corazongana.SetActive(true);
                        //corazonextra.SetActive(true);

						aux=PlayerPrefs.GetInt("corazonestest");
						aux++;
						print ("Auxiliar : " + aux);
						PlayerPrefs.SetInt("corazonestest",aux);
						//soundManager.playSound("Sound_correcto10", 1);
						corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
							PlayerPrefs.GetInt("corazonestest").ToString();
						//corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
						//	numcorazones.ToString();
                        soundManager.playSound("Sound_GanarVida11", 1);
                        Invoke("mostrarpopexito", 5f);
						//PlayerPrefs.SetInt("corazonesODA2_ODA2",numcorazones);
                    }

					if ((PlayerPrefs.GetInt("corazonestest") < 3  ) && (misionactual == 3) && (PlayerPrefs.GetInt("corazonestest") > 0))
                    {
                        inerleyenda.SetActive(false);
                        //corazonextra.SetActive(false);
						//corazonesreto++;

						corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
							PlayerPrefs.GetInt("corazonestest").ToString();
                        brillo.SetActive(false);
                        corazongana.SetActive(false);
                        popupnoacerto.SetActive(true);
                        soundManager.playSound("Sound_OpenPopUp10", 1);

                    }


                    dragging2 = false;
                    flag = true;
                    //Invoke("revisarcronometro", 0f);
                } //fin de draggin2
                //fin de if active in hierarchy
            } //fin de if






        } //fin de input touch > 0

		//revisar cronometro
		//Invoke("revisarcronometro", 10f);

    }


    private void selmisiones()
    {
        Pantalla6.SetActive(true);
        inerleyenda.SetActive(true);
        //activar cronometro
        //cronometro.StartTimer(30);

        //desactivar todas las misiones
        Mision1Opcion1.SetActive(false);
        Mision1Opcion2.SetActive(false);
        Mision1Opcion3.SetActive(false);

        Mision2Opcion1.SetActive(false);
        Mision2Opcion2.SetActive(false);
        Mision2Opcion3.SetActive(false);

        Mision3Opcion1.SetActive(false);
        Mision3Opcion2.SetActive(false);
        Mision3Opcion3.SetActive(false);

        if ((misionactual == 1) && (banderaopcion == 1)) { 
            Mision1Opcion1.SetActive(true);
            //activar cronometro
            cronometro.StartTimer(30);
        }

        if ((misionactual == 1) && (banderaopcion == 2)) { 
            Mision1Opcion2.SetActive(true);
            cronometro.StartTimer(30);
        }
        if ((misionactual == 1) && (banderaopcion == 3)) { 
            Mision1Opcion3.SetActive(true);
            cronometro.StartTimer(30);
        }

        if ((misionactual == 2) && (banderaopcion == 1)) { 
            Mision2Opcion1.SetActive(true);
            cronometro.StartTimer(30);
        }

        if ((misionactual == 2) && (banderaopcion == 2)) { 
            Mision2Opcion2.SetActive(true);
            cronometro.StartTimer(30);
        }
        if ((misionactual == 2) && (banderaopcion == 3)) { 
            Mision2Opcion3.SetActive(true);
           cronometro.StartTimer(30);
        }

        if ((misionactual == 3) && (banderaopcion == 1)) { 
            Mision3Opcion1.SetActive(true);
           cronometro.StartTimer(30);
        }

        if ((misionactual == 3) && (banderaopcion == 2)) { 
            Mision3Opcion2.SetActive(true);
           cronometro.StartTimer(30);
        }
        if ((misionactual == 3) && (banderaopcion == 3)) { 
            Mision3Opcion3.SetActive(true);
           cronometro.StartTimer(30);
        }


    } //fin de funcion selmisiones

    private void revisarpuntuacion()
    {
        imagen1Opcion2m1.SetActive(false);
        imagen1Opcion1m2.SetActive(false);
        imagen1Opcion2m2.SetActive(false);
        imagen1Opcion1m3.SetActive(false);
        imagen1Opcion2m3.SetActive(false);
        imagen1Opcion3m3.SetActive(false);
        imagen2Opcion1m1.SetActive(false);
        imagen2Opcion2m1.SetActive(false);
        imagen2Opcion3m1.SetActive(false);
        imagen2Opcion2m2.SetActive(false);
        imagen2Opcion3m2.SetActive(false);
        imagen2Opcion1m3.SetActive(false);
        imagen2Opcion2m3.SetActive(false);
        imagen2Opcion3m3.SetActive(false);
        imagen3Opcion1m1.SetActive(false);
        imagen3Opcion3m1.SetActive(false);
        imagen3Opcion1m2.SetActive(false);
        imagen3Opcion2m2.SetActive(false);
        imagen3Opcion3m2.SetActive(false);
        imagen3Opcion1m3.SetActive(false);
        imagen3Opcion2m3.SetActive(false);
        imagen3Opcion3m3.SetActive(false);


		if (PlayerPrefs.GetInt("corazonestest") <= 0)
        {
            //detener cronometro
            cronometro.StopTimer();
            //mostrar pop up perder y reiniciar reto
            inerleyenda.SetActive(false);
            //Pantalla6.SetActive (false);
			PlayerPrefs.SetInt("corazonestest",0);
            popuppierdevidas.SetActive(true);
            soundManager.playSound("Sound_PerderVida10", 1);
            
			corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
				PlayerPrefs.GetInt("corazonestest").ToString();
			

        }


    }


    private void revisarcronometro()
    {
        if (cronometro.remainingTime < 0)
        {
            //prender pop up
            cronometro.StopTimer();
            popuptimeout.SetActive(true);
            soundManager.playSound("Sound_OpenPopUp10", 1);
            
        }
    }

    private void mostrarpopexito()
    {
        cronometro.StopTimer();
        inerleyenda.SetActive(false);
        popupexito.SetActive(true);
        soundManager.playSound("Sound_GanarVida11", 1);
    }


    private void irmision1()
    {
        Pantalla6.SetActive(false);
        inerleyenda.SetActive(false);
        cronometro.StopTimer();
        //activar cronometro
        //cronometro.StartTimer(30);

        //poner corazones en ceros
        //numcorazones = 1;
        //corazonesreto = 1;
        //corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
        corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
			PlayerPrefs.GetInt("corazonestest").ToString();
		//corazonretos.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = corazonesreto.ToString();

		Mision1.SetActive(true);
		//corazonm1.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
		corazonm1.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
			PlayerPrefs.GetInt("corazonestest").ToString();
		Mision2.SetActive(false);
        Mision3.SetActive(false);
        misionactual = 1;
        banderaopcion = Random.Range(1, 4);
        broca = true;

        barena = false;
        bpuerta = false;
        llavem1.SetActive(true);
        picom1.SetActive(true);
        palam1.SetActive(true);
        cronometro.StopTimer();
    }


    private void irmision2()
    {
        cronometro.StopTimer();
        imagen1Opcion1m1.SetActive(false);
        imagen1Opcion3m1.SetActive(false);
        imagen3Opcion2m1.SetActive(false);

        Pantalla6.SetActive(false);
        inerleyenda.SetActive(false);
        Mision1.SetActive(false);
        Mision2.SetActive(true);
        
        //corazonm2.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
        corazonm2.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
			PlayerPrefs.GetInt("corazonestest").ToString();
		Mision3.SetActive(false);
        misionactual = 2;
        banderaopcion = Random.Range(1, 4);
        broca = false;
        barena = true;
        bpuerta = false;
        llavem2.SetActive(true);
        picom2.SetActive(true);
        palam2.SetActive(true);
        cronometro.StopTimer();
    }

    private void irmision3()
    {
        cronometro.StopTimer();
        imagen1Opcion3m2.SetActive(false);
        imagen2Opcion1m2.SetActive(false);
        imagen2Opcion2m2.SetActive(false);

        Pantalla6.SetActive(false);
        inerleyenda.SetActive(false);

        Mision1.SetActive(false);
        Mision2.SetActive(false);
        Mision3.SetActive(true);
		//corazonm3.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = numcorazones.ToString();
		corazonm3.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
			PlayerPrefs.GetInt("corazonestest").ToString();
		misionactual = 3;
        banderaopcion = Random.Range(1, 4);
        broca = false;
        barena = false;
        bpuerta = true;
        llavem3.SetActive(true);
        picom3.SetActive(true);
        palam3.SetActive(true);
        cronometro.StopTimer();
    }

    private void iniciar()
    {
        soundManager.playSound("Sound_OpenPopUp10", 1);
        popupr2.SetActive(true);

		corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text =
			PlayerPrefs.GetInt("corazonestest").ToString();
		//corazon.transform.FindChild("marcadorcorazon").GetComponent<Text>().text = PlayerPrefs.GetInt("corazonestest").ToString();
        
    }
} //fin de clase